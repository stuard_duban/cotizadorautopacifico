<?php

require_once($_PATH_SERVIDOR . "/Includes/Herramientas.php");
require_once($_PATH_SERVIDOR . "Config.php");


require_once("Cotizador.php");

$obj_cotizador = new Cotizador();

$_obj_interfaz->adicionarFooterJS("Miscelanea.js");
$_obj_interfaz->adicionarFooterJS("validadores_placas.js");
$_obj_interfaz->adicionarFooterJS("cotizacion.js");

$datos['accion'] = $datos['accion'].$_SESSION['tipo_usuario'] ;

switch($datos['accion'])
{
	case 'fm_editar'.$obj_cotizador->validarAcceso(array("H","R","AD","C")):    //Antes estaba asi: validarAcceso(array("C")), se cambio mientras se trabaja en la nueva gestion de usuarios
        $_SESSION['modulo_activo']="editar";
		$valores = $obj_cotizador->obtenerDatosUsuario($datos['usu_id']);
		if( !is_array($valores))
		{
			$opciones['CADENA']= $idi_despliegue['usuarios_1_cadena'];
			$opciones['ACCION']=0;
			$datos['mensaje'] = $_obj_mensajes->crearMensaje($valores,$opciones);
		}
		$contenido = $obj_cotizador->abrirFormularioEditar($datos,$valores);
		$_obj_interfaz->asignarContenido($contenido);
		break;

	case 'editar'.$obj_cotizador->validarAcceso(array("C","H","R")):
		$resultado = $obj_cotizador->actualizarUsuarioGestion($datos);
		if( $resultado==1)
		{
			$enlace = "index.php?m=usuarios&accion=gestion&tipo_gestion=2&usu_busqueda=".$datos['usu_busqueda']."&msg=".$resultado;
            //DM - 2014-07-29 issue 5040
            if( $datos['est_id'] != '')
            {
                $enlace .= '&est_id='.$datos['est_id'];
            }
			header('Location: '.$enlace);
            exit;
		}
		else
		{
			$opciones['ACCION']=2;
			$opciones['CADENA']= $idi_despliegue['usuarios_1_cadena'];
			$datos['mensaje']= $_obj_mensajes->crearMensaje($resultado,$opciones);
			$valores = $obj_cotizador->obtenerDatosUsuario($datos['usu_id']);
			$contenido = $obj_cotizador->abrirFormularioEditar($datos,$valores);
			$_obj_interfaz->asignarContenido($contenido);
		}
		break;

	//MAOH - 25 Sep 2012 - Incidencia 1761
	case 'infoCotizadorCedula':
		$datos['plantilla'] = 'principal';
		$contenido = $obj_cotizador->abrirFormularioHome($datos);
    	//AMP Cambio a modal de INSPINIA
		 $_obj_interfaz->asignarContenido($contenido);
		break;

	case 'buscarPlacas':
		$datos['plantilla'] = 'principal';
		$contenido = $obj_cotizador->buscarPlacas($datos);
		print_r($contenido);

		exit;
		break;
	
	case 'generarPdf':
		$datos['plantilla'] = 'principal';
		$contenido = $obj_cotizador->generarPdf($datos);
		$_obj_interfaz->asignarContenido($contenido);

		break;
	
	case 'guardarPdf':
		$datos['plantilla'] = 'principal';
		$contenido = $obj_cotizador->guardarPdf($datos);
		print_r($contenido);

		exit;
		break;

	case 'eliminarPDFSPorAntiguedad':
		$datos['plantilla'] = 'principal';
		$contenido = $obj_cotizador->eliminarPDFSPorAntiguedad($datos);
		print_r($contenido);

		exit;
		break;

	case 'obtenerModelos':
		$datos['plantilla'] = 'principal';
		$contenido = $obj_cotizador->obtenerModelos($datos);
		print_r($contenido);

		exit;
		break;

	case 'formularioCotizar':
		$datos['plantilla'] = 'principal';
		$contenido = $obj_cotizador->abrirFormularioCotizacion($datos);
		$_obj_interfaz->asignarContenido($contenido);
		break;

	//Formulario de autenticacion
	default:
	  $datos['plantilla'] = 'principal';
			$contenido = $obj_cotizador->abrirFormularioHome($datos);
			$_obj_interfaz->asignarContenido($contenido);
		break;
}//Fin de switch($datos['accion'])
?>
