<?php

/**
 * 	Nombre: Cotizador.php
 * 	Descripción: Maneja lo referente a las cotizaciones del sistema
 */
class Cotizador {

    var $_plantillas = "";
    var $_herramientas = null;

    function __construct() {
        global $_PATH_SERVIDOR;

        $this->_plantillas = $_PATH_SERVIDOR . "Cotizador/Plantillas";
        $this->_herramientas = new Herramientas;
    }//Fin de __construct()

    function abrirMsgSoporte($datos) {
        global $_PATH_IMAGENES, $_PATH_WEB, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/msg_soporte.html");

        Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);

        Interfaz::asignarToken("btn_cerrar", $idi_despliegue['btn_cerrar'], $contenido);

        return $contenido;
    }

//Fin de abrirMsgSoporte()

    public function abrirFormularioIngformacion($datos) {
        global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $idi_despliegue;

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_informacion.html");

        $boton_login = "iniciar-sesion.jpg";
        $boton_enviar = "enviar.jpg";
        $boton_enviar_2 = "enviar02.jpg";


        if ($_SESSION['idi_id'] == 2) {
            $boton_login = "iniciar-sesion-ingles.jpg";
            $boton_enviar = "enviar-ingles.jpg";
            $boton_enviar_2 = "enviar02-ingles.jpg";
        }
        Interfaz::asignarToken("boton_login", $boton_login, $contenido);
        Interfaz::asignarToken("boton_enviar", $boton_enviar, $contenido);
        Interfaz::asignarToken("boton_enviar_2", $boton_enviar_2, $contenido);



        Interfaz::asignarToken("label_usuario", $idi_despliegue['label_usuario'], $contenido);
        Interfaz::asignarToken("label_contrasenha", $idi_despliegue['label_contrasenha'], $contenido);
        Interfaz::asignarToken("label_olvido_contrasenha", $idi_despliegue['label_olvido_contrasenha'], $contenido);

        Interfaz::asignarToken("label_nombre_establecimiento", $idi_despliegue['label_nombre_establecimiento'], $contenido);
        Interfaz::asignarToken("label_nombre", $idi_despliegue['label_nombre'], $contenido);
        Interfaz::asignarToken("label_telefono", $idi_despliegue['label_telefono'], $contenido);
        Interfaz::asignarToken("label_correo", $idi_despliegue['label_correo'], $contenido);
        Interfaz::asignarToken("label_pms", $idi_despliegue['label_pms'], $contenido);
        Interfaz::asignarToken("label_captcha", $idi_despliegue['label_captcha'], $contenido);



        Interfaz::asignarToken("usu_login", $datos['usu_login'], $contenido);
        Interfaz::asignarToken("inf_nombre_establecimiento", $datos['inf_nombre_establecimiento'], $contenido);
        Interfaz::asignarToken("inf_nombre", $datos['inf_nombre'], $contenido);
        Interfaz::asignarToken("inf_correo", $datos['inf_correo'], $contenido);
        Interfaz::asignarToken("inf_telefono", $datos['inf_telefono'], $contenido);
        Interfaz::asignarToken("inf_pms", $datos['inf_pms'], $contenido);
        Interfaz::asignarToken("imagenes", $_PATH_IMAGENES, $contenido);
        Interfaz::asignarToken("path_web", $_PATH_WEB, $contenido);

        Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);

        return $contenido;
    }

//Fin de abrirFormularioIngformacion()


/** abrirFormularioHome
 * parametro: $datos
 * autor : JCB - DESARROLLO AUTOPACIFICO
 * descripcion: ABRE EL CONTENIDO DE LA PLANTILAL FM_HOME
 **/
public function abrirFormularioHome($datos) {
  global $_PATH_IMAGENES, $_PATH_WEB, $_opciones;

  $boton_procesar = "COTIZAR";

  $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_home.html");
  $msg_titulo = 'Cotizador';
  $msg_descripcion = "Bienvenido al sistema de Cotizaciones en línea de Autopacifico";
  $input_nombre = 'NOMBRE Y APELLIDO';
  $input_placa = 'PLACA';
  $input_anho = 'AÑO';

  $conexion = $this->abrirConexionSQL();

  $sql = "EXEC sp_ap_cot_consulta_KM";
  $stmt = sqlsrv_prepare($conexion, $sql);
  if (!sqlsrv_execute($stmt)) {
    die( print_r( sqlsrv_errors(), true));
  }

  $selectKilometraje = "<select id=\"kilometraje\" class=\"selectpicker\" name=\"kilometraje\" data-live-search=\"true\">";
  $selectKilometraje .= "<option value='-1' selected='selected'>---</option>";
  while($kilometraje = sqlsrv_fetch_object($stmt)){
    $selectKilometraje .= "<option value=\"$kilometraje->kilometraje\" data-tokens=\"$kilometraje->kilometraje\">".number_format( round($kilometraje->kilometraje, 2), 0, ",", "." )."</option>";
  }
  $selectKilometraje .= "</select>";
  

  $sql = "EXEC sp_ap_cot_lista_talleres";
  $stmt = sqlsrv_prepare($conexion, $sql);
  if (!sqlsrv_execute($stmt)) {
    die( print_r( sqlsrv_errors(), true));
  }

  
  $selectTalleres = "<select id=\"bodega\" class=\"selectpicker\" name=\"bodega\" data-live-search=\"true\">";
  $selectTalleres .= "<option value='-1' selected='selected'>---</option>";
  while($taller = sqlsrv_fetch_object($stmt)){
    $selectTalleres .= "<option value=\"$taller->bodega\" data-descripcion=\"$taller->descripcion\"
    data-tokens=\"$taller->descripcion\">$taller->descripcion</option>";
  }
  $selectTalleres .= "</select>";
  


  $img_ruta = "Includes/Imagenes";

  Interfaz::asignarToken("imagenes", $img_ruta, $contenido);
  Interfaz::asignarToken("boton_procesar", $boton_procesar, $contenido);
  Interfaz::asignarToken("label_cedula", utf8_encode('No. C�dula'), $contenido);
  Interfaz::asignarToken("label_valor", utf8_encode('Valor aporte'), $contenido);
  Interfaz::asignarToken("label_placa", 'No. Placa', $contenido);
  Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
  Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
  Interfaz::asignarToken("input_nombre", $input_nombre, $contenido);
  Interfaz::asignarToken("input_placa", $input_placa, $contenido);

  Interfaz::asignarToken("label_linea", 'LÍNEA', $contenido);
  Interfaz::asignarToken("select_linea", $selectModelos, $contenido);

  Interfaz::asignarToken("label_kilometraje", 'KILOMETRAJE', $contenido);
  Interfaz::asignarToken("select_kilometraje", $selectKilometraje, $contenido);

  Interfaz::asignarToken("label_talleres", 'TALLER', $contenido);
  Interfaz::asignarToken("select_talleres", $selectTalleres, $contenido);

  Interfaz::asignarToken("input_anho", $input_anho, $contenido);
  Interfaz::asignarToken("checkbox_identificacion", $ckeckBox, $contenido);


  return $contenido;
}


  /** abrirFormularioCotizacion
 * parametro: $datos
 * autor : StuardRomero - DESARROLLO AUTOPACIFICO
 * descripcion: ABRE EL CONTENIDO DE LA PLANTILAL FM_HOME
 **/
public function abrirFormularioCotizacion($datos) {
  global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $_porcentaje_iva;

  $boton_procesar = "COTIZAR";

  $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_formulario_cotizacion.html");
  $msg_titulo = 'Cotizador';
  $msg_descripcion = "Bienvenido al sistema de Cotizaciones en línea de Autopacifico";
  $label_nombre = 'Nombre del cliente:';
  $label_placa = 'Placa del vehiculo:';
  $label_linea = 'Línea:';
  $label_kilometraje = 'Kilometraje:';
  $label_anho = 'Año:';

  $kilometraje = number_format( round($datos['kilometraje'], 2), 0, ",", "." );

  $titulo_mantenimiento_basico = "Mantenimiento Básico";

  $conexion = $this->abrirConexionSQL();

  setlocale(LC_MONETARY, 'es_CO');
  $stmt = sqlsrv_prepare($conexion, "EXEC sp_ap_cot_consulta_planes_mantenimiento_id_KM ?, ?, ?", array($datos['linea'], $datos['kilometraje'], $datos['bodega']), array( "Scrollable" => 'buffered' ));
  if (!sqlsrv_execute($stmt)) {
    die( print_r( sqlsrv_errors(), false));
  } 

  if (sqlsrv_num_rows($stmt) !== 0) {
    $mantenimiento_basico = '
    <tr class="titulo-item">
      <td>
        <input type="checkbox" class="form-check-input descuento">
      </td>
      <td>
        <div class="card">
          <div class="card-header" id="headingOne">
            <button class="btn btn-link" data-toggle="collapse" data-target="#mantenimientoBasico" aria-expanded="true" aria-controls="mantenimientoBasico">
              <i class="fa fa-plus"></i>
            </button>
            <h5 class="mb-0">
              Mantenimiento de '.$kilometraje.' km
            </h5>
          </div>
        </div>
      </td>
      <td></td>
      <td>1</td>
      <td>0%</td>
      <td>'.$_porcentaje_iva.'%</td>
      <td';

    $tabla = '
      </td>
    </tr>
    <tr id="mantenimientoBasico" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
      <td colspan="100%" style="padding: 0px 27px;">
        <table id="detalleMantenimientoBasico">
          <thead>
            <tr>
              <th></th>
              <th>Descripción</th>
              <th>Valor UNT</th>
              <th>Cant</th>
              <th>%Descuento</th>
              <th>IVA</th>
              <th>Valor - Descuento + IVA</th>
            </tr>
          </thead>
          <tbody>';

    $subtotal_con_iva = 0;
    while ($plan_mantenimiento = sqlsrv_fetch_object($stmt)) {
      $monto_descuento = $plan_mantenimiento->valor_sin_iva * ($plan_mantenimiento->porce_dcto_mo / 100);
      $iva = ($plan_mantenimiento->valor_sin_iva - $monto_descuento) * ($plan_mantenimiento->porcentaje_iva / 100);
      $subtotal_monto_iva += $iva;

      $valor_sin_iva = $plan_mantenimiento->valor_sin_iva;
      $valor_con_iva = $plan_mantenimiento->valor_con_iva;

      $clase_disponibilidad = '';
      if ($plan_mantenimiento->tipo === 'R') {
        $monto_descuento = $plan_mantenimiento->valor_sin_iva * $plan_mantenimiento->cantidad * ($plan_mantenimiento->porce_dcto_rep / 100);
        $iva = ($plan_mantenimiento->valor_sin_iva * $plan_mantenimiento->cantidad - $monto_descuento) * ($plan_mantenimiento->porcentaje_iva / 100);
        $subtotal_monto_iva += $iva;

        $valor_sin_iva = $plan_mantenimiento->valor_sin_iva * $plan_mantenimiento->cantidad;
        $valor_con_iva = $plan_mantenimiento->valor_con_iva * $plan_mantenimiento->cantidad;

        if ($plan_mantenimiento->stock == 0) {
          $clase_disponibilidad = 'bg-danger';
        } else if ($plan_mantenimiento->stock != 0 && $plan_mantenimiento->stock < $plan_mantenimiento->cantidad) {
          $clase_disponibilidad = 'bg-warning';
        }
      }

      
      $subtotal_sin_iva += $valor_sin_iva;
      $subtotal_con_iva += $valor_con_iva;

      $tabla .= '<tr class="'.$clase_disponibilidad.'">
        <td></td>
        <td>
          '.$plan_mantenimiento->descripcion.'
        </td>
        <td data-valor_sin_iva='.$plan_mantenimiento->valor_sin_iva.'>$'.number_format( round($plan_mantenimiento->valor_sin_iva, 0), 0, ",", "." ).'</td>
        <td>
          '.$plan_mantenimiento->cantidad.'
        </td>
        <td data-porcentaje_descuento='.$pack->porce_dcto_mo.'>'.(is_null($pack->porce_dcto_mo) ? 0 : $pack->porce_dcto_mo).'%</td>
        <td>
          '.$plan_mantenimiento->porcentaje_iva.'%
        </td>
        <td data-valor_con_iva='.$valor_con_iva.'>$'.number_format( round($valor_con_iva, 0), 0, ",", "." ).'</td>
      </tr>';
    }

    $tabla .= "
            </tbody>
          </table>
        </td>
      </tr>
    </div>";

    $mantenimiento_basico .= ' data-descuento=0 data-sub_monto_iva='.$subtotal_monto_iva.' data-monto_con_iva="'.$subtotal_con_iva.'" data-monto_sin_iva="'.$subtotal_sin_iva.'" data-monto="'.$subtotal_con_iva.'">$'.number_format( $subtotal_con_iva, 0, ",", "." ).$tabla;
  }
  
  $stmt = sqlsrv_prepare($conexion, 'EXEC sp_ap_cot_consulta_packs_id_ano_bas ?, ?', array($datos['linea'], $datos['bodega']), array( "Scrollable" => 'buffered' ));

  if (!sqlsrv_execute($stmt)) {
    die( print_r( sqlsrv_errors(), false));
  } else {
    $packs = $this->generarPacks($stmt, true);
    $tabla = $packs['tabla'];
  }

  $mantenimiento_basico .= $tabla;

  $stmt = sqlsrv_prepare($conexion, 'EXEC sp_ap_cot_consulta_packs_id_ano_ind ?, ?', array($datos['linea'], $datos['bodega']), array( "Scrollable" => 'buffered' ));

  
  if (!sqlsrv_execute($stmt)) {
    die( print_r( sqlsrv_errors(), true));
  } else {
    $packs = $this->generarPacks($stmt, true);
    $tabla = $packs['tabla'];
  }

  $mantenimiento_indispensable = $tabla;
  
  $stmt = sqlsrv_prepare($conexion, 'EXEC sp_ap_cot_consulta_packs_id_ano_des ?, ?', array($datos['linea'], $datos['bodega']), array("Scrollable"=>"buffered"));

  if (!sqlsrv_execute($stmt)) {
    die( print_r( sqlsrv_errors(), true));
  } else {
    $packs = $this->generarPacks($stmt, true);
    
    $tabla = $packs['tabla'];
  }

  $mantenimiento_desgaste = $tabla;


  $img_ruta = "Includes/Imagenes";

  Interfaz::asignarToken("imagenes", $img_ruta, $contenido);
  Interfaz::asignarToken("boton_procesar", $boton_procesar, $contenido);
  Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
  Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);

  Interfaz::asignarToken("label_nombre", $label_nombre, $contenido);
  Interfaz::asignarToken("nombre_cliente", $datos['nombre'], $contenido);


  Interfaz::asignarToken("label_placa", $label_placa, $contenido);
  Interfaz::asignarToken("placa", $datos['placa'], $contenido);

  Interfaz::asignarToken("label_linea", $label_linea, $contenido);
  Interfaz::asignarToken("linea", $datos['linea_descripcion'], $contenido);

  Interfaz::asignarToken("label_anho", $label_anho, $contenido);
  Interfaz::asignarToken("anho", $datos['anho'], $contenido);

  Interfaz::asignarToken("label_kilometraje", $label_kilometraje, $contenido);
  Interfaz::asignarToken("kilometraje", $kilometraje, $contenido);

  Interfaz::asignarToken("titulo_mantenimiento_basico", $titulo_mantenimiento_basico, $contenido);
  Interfaz::asignarToken("mantenimiento_basico", $mantenimiento_basico, $contenido);

  Interfaz::asignarToken("titulo_mantenimiento_indispensable", 'Mantenimiento Indispensable', $contenido);
  Interfaz::asignarToken("mantenimiento_indispensable", $mantenimiento_indispensable, $contenido);

  Interfaz::asignarToken("titulo_mantenimiento_desgaste", 'Mantenimiento Desgaste', $contenido);
  Interfaz::asignarToken("mantenimiento_desgaste", $mantenimiento_desgaste, $contenido);

  Interfaz::asignarToken("label_kilometraje", 'KILOMETRAJE', $contenido);
  Interfaz::asignarToken("select_kilometraje", $selectKilometraje, $contenido);

  Interfaz::asignarToken("label_anho", $label_anho, $contenido);
  Interfaz::asignarToken("checkbox_identificacion", $ckeckBox, $contenido);


  return $contenido;
}

  private function generarPacks($declaracion, $subitems=false) {
    global $_porcentaje_iva;

    $posee_packs = false;
    $tabla = '';
    $acordeon = '';
    $numero_grupo = 0;
    $subtotal_con_iva = 0;
    $subtotal_monto_iva = 0;
    $subtotal_con_iva_descuento = 0;
    
    $numero_item = 0;
    $total_packs = sqlsrv_num_rows($declaracion);

    if ($total_packs == 0) {
      return [
        'tabla' => '<div class="packs-status ocultar-imprimir">No hay packs disponibles</div>',
      ];
    }

    if ($subitems) { 
      while ($pack = sqlsrv_fetch_object($declaracion)) {
        $numero_item++;
        $posee_packs = true;
        $valor_sin_iva = number_format( round($pack->valor_sin_iva, 0), 0, ",", "." );
        $valor_con_iva = number_format( round($pack->valor_con_iva, 0), 0, ",", "." );

        $monto_descuento = $pack->valor_sin_iva * ($pack->porce_dcto_mo / 100);
        $iva = ($pack->valor_sin_iva - $monto_descuento) * ($pack->porcentaje_iva / 100);
        $valor_descuento_iva = $pack->valor_sin_iva - $monto_descuento + $iva;

        //Porcentaje descuento de item hijo
        $porcentaje_descuento = $pack->porce_dcto_mo;

        $clase_disponibilidad = '';
        if ($pack->clase_operacion === 'R') {
          $monto_descuento = $pack->valor_sin_iva * $pack->cantidad * ($pack->porce_dcto_rep / 100);
          $iva = ($pack->valor_sin_iva * $pack->cantidad - $monto_descuento) * ($pack->porcentaje_iva / 100);
          $valor_descuento_iva = $pack->valor_sin_iva * $pack->cantidad - $monto_descuento + $iva;

          $porcentaje_descuento = $pack->porce_dcto_rep;

          if ($pack->stock == 0) {
            $clase_disponibilidad = 'bg-danger';
          } else if ($pack->stock != 0 && $pack->stock < $pack->cantidad) {
            $clase_disponibilidad = 'bg-warning';
          }
        }

        if ($pack->nombre !== $grupo_actual) {
          if ($numero_grupo !== 0) {
            $tabla .= "
                    </tbody>
                  </table>
                </td>
              </tr>
            </div>";
  
            $acordeon .= ' data-descuento='.$subtotal_monto_descuento.' data-sub_monto_iva="'.$subtotal_monto_iva.'" data-monto_con_iva="'.$subtotal_con_iva.'" data-monto_sin_iva="'.$subtotal_sin_iva.'" data-monto="'.$subtotal_con_iva_descuento.'">$'.number_format( $subtotal_con_iva_descuento, 0, ",", "." ).$tabla;
            $subtotal_sin_iva = 0;
            $subtotal_monto_iva = 0;
            $subtotal_con_iva = 0;
            $subtotal_con_iva_descuento = 0;
            $subtotal_monto_descuento = 0;
          }
  
          $grupo_actual = $pack->nombre;
          
          $id_grupo = str_replace(' ', '', $pack->des_grupo.$numero_grupo); 
          $acordeon .= '
          <tr class="titulo-item">
            <td>
              <input type="checkbox" class="form-check-input">
            </td>
            <td>
              <div class="card">
                <div class="card-header" id="headingOne">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#'.$id_grupo.'" aria-expanded="true" aria-controls="mantenimientoBasico">
                    <i class="fa fa-plus"></i>
                  </button>
                  <h5 class="mb-0">
                    '.$pack->nombre.'
                  </h5>
                </div>
                </div>
            </td>
            <td></td>
            <td>1</td>
            <td data-total_porcentaje_descuento></td>
            <td>'.$_porcentaje_iva.'%</td>
            <td';
  
            $tabla = '
              </td>
            </tr>
            <tr id="'.$id_grupo.'" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
              <td colspan="100%" style="padding: 0px 27px;">
                <table id="detallePacksDesgastes">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Descripción</th>
                      <th>Valor UNT</th>
                      <th>Cant</th>
                      <th>% Descuento</th>
                      <th>IVA</th>
                      <th>Valor - Descuento + IVA</th>
                    </tr>
                  </thead>
                  <tbody>
                  <tr class="'.$clase_disponibilidad.'">
                    <td></td>
                    <td>'.$pack->descripcion.'</td>
                    <td data-valor_sin_iva='.$pack->valor_sin_iva.'>$'.$valor_sin_iva.'</td>
                    <td>'.$pack->cantidad.'</td>
                    <td data-porcentaje_descuento='.$porcentaje_descuento.' data-monto_descuento='.$monto_descuento.'>'.$porcentaje_descuento.'%</td>
                    <td data-sub_monto_iva='.$iva.'>'.$pack->porcentaje_iva.'%</td>
                    <td data-valor_con_iva='.$valor_descuento_iva.'>$'.number_format( $valor_descuento_iva, 0, ",", "." ).'</td>
                  </tr>';
  
          $numero_grupo++;
        } else {
  
          $tabla .= '<tr class="'.$clase_disponibilidad.'">
            <td></td>
            <td>'.$pack->descripcion.'</td>
            <td data-valor_sin_iva='.$pack->valor_sin_iva.'>$'.$valor_sin_iva.'</td>
            <td>'.$pack->cantidad.'</td>
            <td data-porcentaje_descuento='.$porcentaje_descuento.' data-monto_descuento='.$monto_descuento.'>'.$porcentaje_descuento.'%</td>
            <td data-sub_monto_iva='.$iva.'>'.$pack->porcentaje_iva.'%</td>
            <td data-valor_con_iva='.$valor_descuento_iva.'>$'.number_format( $valor_descuento_iva, 0, ",", "." ).'</td>
          </tr>';
        }
        
        //Si es tipo R se multiplica valor neto por cantidad
        if ($pack->clase_operacion === 'R') {
          $subtotal_sin_iva += ($pack->valor_sin_iva * $pack->cantidad);
        } else {
          $subtotal_sin_iva += $pack->valor_sin_iva;
        }

        $subtotal_monto_iva += $iva;
        $subtotal_con_iva += $pack->valor_con_iva;
        $subtotal_con_iva_descuento += $valor_descuento_iva;
        $subtotal_monto_descuento += $monto_descuento;
  
        if ($numero_item === $total_packs) {
          $tabla .= "
                  </tbody>
                </table>
              </td>
            </tr>
          </div>";
  
          $acordeon .= ' data-descuento='.$subtotal_monto_descuento.' data-sub_monto_iva='.$subtotal_monto_iva.' data-monto_con_iva="'.$subtotal_con_iva.'" data-monto_sin_iva="'.$subtotal_sin_iva.'" data-monto="'.$subtotal_con_iva_descuento.'">$'.number_format( $subtotal_con_iva_descuento, 0, ",", "." ).$tabla;
          $subtotal_sin_iva = 0;
          $subtotal_monto_iva = 0;
          $subtotal_con_iva = 0;
          $subtotal_con_iva_descuento = 0;
          $subtotal_monto_descuento = 0;
        }
      }

      return [
        'posee_packs' => $posee_packs,
        'tabla' => $acordeon,
        'valor_con_iva' => $valor_con_iva,
        'valor_sin_iva' => $valor_sin_iva
      ];
    } else {
      while ($pack = sqlsrv_fetch_object($declaracion)) {
        $posee_packs = true;
        $valor_sin_iva = number_format( round($pack->valor_sin_iva, 2), 0, ",", "." );
        $valor_con_iva = number_format( round($pack->valor_con_iva, 2), 0, ",", "." );

        $monto_descuento = $pack->valor_sin_iva * ($pack->porce_dcto_mo / 100);
        $iva = ($pack->valor_sin_iva - $monto_descuento) * ($pack->porcentaje_iva / 100);
        $valor_descuento_iva = $pack->valor_sin_iva - $monto_descuento + $iva;

        //Porcentaje descuento mano de obra
        $porcentaje_descuento = $pack->porce_dcto_mo;
        $clase_disponibilidad = '';
        if ($pack->clase_operacion === 'R') {
          $monto_descuento = $pack->valor_sin_iva * $pack->cantidad * ($pack->porce_dcto_rep / 100);
          $iva = ($pack->valor_sin_iva * $pack->cantidad - $monto_descuento) * ($pack->porcentaje_iva / 100);
          $valor_descuento_iva = $pack->valor_sin_iva * $pack->cantidad - $monto_descuento + $iva;

          //Porcentaje descuento repuesto
          $porcentaje_descuento = $pack->porce_dcto_rep;

          if ($pack->stock == 0) {
            $clase_disponibilidad = 'bg-danger';
          } else if ($pack->stock != 0 && $pack->stock < $pack->cantidad) {
            $clase_disponibilidad = 'bg-warning';
          }
        }
    
        $tabla .= '<tr class="'.$clase_disponibilidad.'">
          <td><input type="checkbox" class="form-check-input descuento"></td>
          <td>'.$pack->descripcion.'</td>
          <td data-valor_sin_iva='.$pack->valor_sin_iva.'>$'.$valor_sin_iva.'</td>
          <td>'.$pack->cantidad.'</td>
          <td data-porcentaje_descuento='.$porcentaje_descuento.' data-monto_descuento='.$monto_descuento.'>'.$porcentaje_descuento.'%</td>
          <td>'.$pack->porcentaje_iva.'%</td>
          <td data-valor_con_iva='.$valor_descuento_iva.'>$'.number_format( $valor_descuento_iva, 0, ",", "." ).'</td>
        </tr>';
      }

      return [
        'posee_packs' => $posee_packs,
        'tabla' => $tabla,
        'valor_con_iva' => $valor_con_iva,
        'valor_sin_iva' => $valor_sin_iva
      ];
    }
  }


  /** abrirConexionSQL
   * parametro: $servidor, $bd, $usuario, $pass
   * autor : Stuard Romero - DESARROLLO AUTOPACIFICO
   * descripcion: Abre y realiza la conexion del servidor sqlserver
   **/
  private function abrirConexionSQL() {
    global $_PATH_IMAGENES, $_PATH_WEB, $_opciones, $_host_sql_server, $_db_sql_server, $_user_db_sql_server, $_pass_db_sql_server;
    
    try {
      
      $opcionesConexion = array(
        "database" => $_db_sql_server,
        "uid" => $_user_db_sql_server,
        "pwd" => $_pass_db_sql_server
      );

      $conexion = sqlsrv_connect($_host_sql_server, $opcionesConexion);

      if ($conexion === false) {
        echo '<pre>';
        print_r(sqlsrv_errors());
        echo '</pre>';
      }

    } catch (PDOException $exception) {
        echo "<pre>";
        var_dump($exception);
        echo "</pre>";
    }

    return $conexion;
  }


  public function buscarPlacas() {
    $conexion = $this->abrirConexionSQL();
    $placa = $_POST['placa'];

    $sql = "EXEC sp_ap_cot_consulta_placa $placa";

    $stmt = sqlsrv_prepare($conexion, $sql);

    if (!sqlsrv_execute($stmt)) {
      die( print_r( sqlsrv_errors(), true));
    }

    $resultado = sqlsrv_fetch_array($stmt);

    $resultado = array_filter($resultado, function($value, $key) {
      return $key !== '' & isset($value);
    }, ARRAY_FILTER_USE_BOTH);

    if (count($resultado) > 0 && $resultado[0] !== '' && $resultado[1] !== '') {
      $sql = "EXEC sp_ap_cot_consulta_modano ".$resultado[1];
      
      $stmt = sqlsrv_prepare($conexion, $sql);
      
      if (!sqlsrv_execute($stmt)) {
        die( print_r( sqlsrv_errors(), true));
      }
      
      $modano = sqlsrv_fetch_array($stmt);


      $es_modano_invalido = is_null($modano[0]) || is_null($modano[1]) || $modano[0] === '' || $modano[1] === '';

      if ($es_modano_invalido) {
        // $this->enviarNotificacion('La siguiente placa no se encuentra registrada en el sistema: '.$placa);
        return json_encode([
          'mensaje' => 'La placa no se encuentra registrada.',
          'resultado' => null,
          'trae_registros' => false
        ]);
      }

      return json_encode([
        'mensaje' => 'La placa se encuentra registrada.',
        'trae_registros' => true,
        "id_modelo" => $modano[0],
        "anho" => $modano[1],
        "descripcion_linea" => $modano[2],
      ]);

    }
    
    // $this->enviarNotificacion('La siguiente placa no se encuentra registrada en el sistema: '.$placa);

    return json_encode([
      'mensaje' => 'La placa no se encuentra registrada.',
      'resultado' => null,
      'trae_registros' => false
    ]);
  }

  public function eliminarPDFSPorAntiguedad($datos) {
    $conexion = $this->abrirConexionSQL();

    $datos['periodo'] = !isset($datos['periodo']) ? 'mes' : $datos['periodo'];
    $datos['cantidad'] = !isset($datos['cantidad']) ? 1 : $datos['cantidad'];

    if ($datos['periodo'] = 'mes') {
      $sql = "SELECT * FROM ap_coti_log_cotizaciones WHERE fecha_cotizacion < DATEADD(MONTH, ?, GETDATE())";

      $stmt = sqlsrv_prepare($conexion, $sql, array(-(int)$datos['cantidad']), array( "Scrollable" => 'buffered' ));

      if (!sqlsrv_execute($stmt)) {
        return json_encode(sqlsrv_errors());
      }

      while ($pdf = sqlsrv_fetch_object($stmt)) {
        unlink($pdf->ruta);
        $pdfsAEliminar[] = $pdf->id_cotizacion;
      }

      if (!isset($pdfsAEliminar)) {
        return json_encode([
          'mensaje' => 'No se elimino ningun archivo.',
          'resultado' => true
        ]);
      }

      $sql = "DELETE FROM ap_coti_log_cotizaciones WHERE id_cotizacion IN (" . implode(',', $pdfsAEliminar) . ")";

      $stmt = sqlsrv_query( $conexion, $sql);
      if(!$stmt) {
        return json_encode(sqlsrv_errors());
      }

      $no_db_files = glob("/var/www/html/cotizadorpruebas/Cotizador/uploads/*.pdf");

      $months = (int)$datos['cantidad'] * 30 * 24 * 3600;
      // 1 dia
      //$months = 24 * 3600;


      foreach ($no_db_files as $archivo) {
        //En caso de no haber registros de los archivos en base de datos
        if((time()-filemtime($archivo)) > $months)
        {
          if(is_file($archivo)) {
            unlink($archivo);
          }
        }
      }
      
    }

    return json_encode([
      'mensaje' => 'Los PDFs y sus registros fueron eliminados exitosamente.',
      'resultado' => true
    ]);
  }

  public function generarPdf($datos) {
    global $_PATH_SERVIDOR;

    $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_generar_pdf.html");

    $mensaje_observaciones = "Observaciones acerca de esta cotización: Este documento es emitido a partir de una solicitud de cotización telefónica o como consecuencia de una inspección visual del vehículo sin efectuar ningún tipo de desarme, de igual manera se efectúa sin entrar en detalles técnicos particulares del vehículo, ni teniendo en cuenta variaciones sin previo aviso que hubiese podido efectuar el fabricante o usuario del vehículo, por lo tanto: si al momento de efectuar los trabajos: antes o durante la ejecución los mismos, posterior a la autorización de esta cotización por parte del cliente, se detecta que alguno de los elementos, partes o repuestos cotizados, no corresponden al vehículo, será notificada esta novedad al cliente antes de proceder para obtener su aprobación, entendiéndose así que al no corresponder el elemento cotizado inicialmente al vehículo, no existe por parte de Autopacifico S.A. obligación alguna de mantener el precio de la parte/repuesto y su mano de obra, al ser un componente distinto con características diferentes a las del elemento cotizado inicialmente. De igual manera, en cuanto a los valores cotizados de mano de obra, estos se encuentran estimados basándose en un vehículo original/estándar, por lo tanto: si al momento de efectuar los trabajos se encuentra: modificaciones en el vehículo o accesorios que impliquen un trabajo adicional para poder efectuar los trabajos cotizados o daños no evidenciados/reportados inicialmente o dificultades derivadas del uso/antigüedad del vehículo tales como fallas en roscas de tornillos, elementos “pegados” por oxido o tiempo, o cualquier condición que implique un trabajo adicional o uso de elementos/suministros adicionales, estos serán cotizados como un servicio adicional al cliente, sin perjuicio de que esto represente un incumplimiento de esta cotización por parte de Autopacifico S.A.. Esta cotización tiene una vigencia de 30 días calendario, sin embargo, el concesionario Autopacifico S.A. se reserva el derecho de ajustar los precios de esta cotización sin previo aviso durante este periodo de tiempo en caso de que por motivos de fuerza mayor existan cambios drásticos en los precios generados por parte de sus proveedores.";

    Interfaz::asignarToken("html_pdf", $datos['pdf'], $contenido);
    Interfaz::asignarToken("observaciones", $mensaje_observaciones, $contenido);
    Interfaz::asignarToken("fecha_cotizacion", date('d-m-Y'), $contenido);



    return $contenido;
  }

  public function guardarPdf($datos) {
    global  $_host_sql_server, $_db_sql_server, $_user_db_sql_server, $_pass_db_sql_server;

    $b64 = $datos['file'];
    $bin = base64_decode($b64, true);
    
    $fecha = date('Y-m-d H:i:s');
    $ruta_pdf = "/var/www/html/cotizadorpruebas/Cotizador/uploads/".$fecha." - ".$datos['placa'].".pdf";


    $pdf = fopen($ruta_pdf, "w");
    $estado = (boolean)fwrite($pdf, $bin);

    $estado &= fclose($pdf);

    if ($estado) {
      $opcionesConexion = array(
        "database" => $_db_sql_server,
        "uid" => $_user_db_sql_server,
        "pwd" => $_pass_db_sql_server
      );
  
      $conexion = sqlsrv_connect($_host_sql_server, $opcionesConexion);
      
      $sql = "INSERT INTO ap_coti_log_cotizaciones (placa, fecha_cotizacion, ruta) VALUES (?, ?, ?)";

      $resultado = sqlsrv_query($conexion, $sql, array($datos['placa'], $fecha, $ruta_pdf));

      if (!$resultado) {
        return json_encode(array(
          'estado' => "fallido",
          'mensaje' => "Ocurrio un error al guardar el registro en la Base de Datos."
        ));
      }

    } else {
      return json_encode(array(
        'estado' => "fallido",
        'mensaje' => "Ocurrio un error al almacenar el PDF en el servidor."
      ));
    }

    return json_encode(array(
      'estado' => "exitoso",
      'mensaje' => "PDF almacenado exitosamente."
    ));

    die();
  }

  public function enviarNotificacion($mensajeNotificacion) {
    global $_PATH_SERVIDOR, $_EMAIL_ADMIN, $_smtp, $_login_smtp, $_clave_smtp, $_puerto_smtp;
    require_once($_PATH_SERVIDOR . "/Includes/PhpMailer/class.phpmailer.php");

    $mail = new PHPMailer();

    $mail->IsSMTP();

    $mail->SMTPDebug = 1;
    
    
    
    $mail->Host = $_smtp;

    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'ssl';
    $mail->Username = $_login_smtp;
    $mail->Password = $_clave_smtp;
    $mail->Port = $_puerto_smtp;

    

    $mail->AddAddress($_EMAIL_ADMIN);

    $mail->From = 'stuard.duban@gmail.com';
    $mail->Subject = 'Placa No existente';

    $mail->Body = $mensajeNotificacion;

    $resultado= $mail->Send();

		if (!$resultado)
		{
      echo $mail->ErrorInfo;
		  return 0;
		}
		else {

		  return 1;
    }

  }

  public function obtenerModelos() {
    $conexion = $this->abrirConexionSQL();
    
    $sql = "EXEC sp_ap_cot_consulta_modelos_sel";
    $stmt = sqlsrv_prepare($conexion, $sql);

    if (!sqlsrv_execute($stmt)) {
      die( print_r( sqlsrv_errors(), true));
    }

    $selectModelos = "<select id=\"linea\" class=\"selectpicker\" name=\"linea\" data-live-search=\"true\">";
    $selectModelos .= "<option value='-1' selected='selected'>---</option>";
    while($modelo = sqlsrv_fetch_object($stmt)){
      $selectModelos .= "<option value=\"$modelo->id\" data-anho=\"$modelo->ano\" data-descripcion=\"$modelo->descripcion\"
      data-tokens=\"$modelo->descripcion\">$modelo->descripcion</option>";
    }
    $selectModelos .= "</select>";

    return json_encode([
      'resultado' => $selectModelos,
      'trae_registros' => true
    ]);
  }

    /** abrirFormularioGetCotizadorCedula
     * parametro: $datos
     * autor : JCB - DESARROLLO AUTOPACIFICO
     * descripcion: abre el formulario con informacion del usuario y la informacion de pago.
     **/
    function abrirFormularioInfoCotizadorCedula($datos) {
    	global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones, $_conexionAuto;

      //Logo
      $img_ruta = "Includes/Imagenes";


      //TITULO
      $msg_titulo = 'Sistema de Cotizador de Facturas';
      //Descripción

    	$boton_procesar = "SIGUIENTE";

    	/* titulos*/
    	$msg_titulo_datoscli = '<h1>Informaci�n personal</h1>';
    	$msg_titulo_datospago = '<h1>Concepto de pago</h1>';

    	/*datos info pago*/

      //Preparando consulta
      $_conexionAuto = Cotizador::abrirConexionSQL();    //Abrir conexion SQL Server


      /*Obtener facturas pendientes*/
      $sql = 'exec sp_ap_obtener_datos_basicos_pago_pse ?';

      $stmt=$_conexionAuto->prepare($sql);

      $cedula = $datos['cli_cedula'];

      $stmt->bindValue(1, $cedula);


      $resultado=$stmt->execute();

      $facturas = $stmt->fetchALL(PDO::FETCH_ASSOC);



      if(key($facturas[0]) != 'Error') {

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_infoCotizadorCedula.html");


        //print_r($facturas);
        $nombre = $facturas[0]['cliente'];
        $telefono = $facturas[0]['celular'];
        $correo = $facturas[0]['mail'];

        $msg_descripcion = "Estimado $nombre, hemos encontrado las siguientes facturas por pagar en nuestro sistema, por favor selecciona las que deseas pagar y haz clic en siguiente:";

        $sql_verificar_factura = "SELECT * FROM factura where fac_estado = 'Pendiente'";
        $verificar_facturas = $_obj_database->obtenerRegistrosAsociativos($sql_verificar_factura);


        //exit;
        /*Verificacion de facturas pendientes*/
        foreach ($facturas as $id_fac => $factu) {
          $facturas[$id_fac]['estado_pse'] = "";
          foreach ($verificar_facturas as $id_verif => $factu_verif) {

            if($factu['Numero'] == $factu_verif['fac_id']) {

              $facturas[$id_fac]['estado_pse'] = "PENDING";

            }
          }
        }

        /*Validacion no muesttre facturas si no hay disponibles*/
        if($facturas[0]['Numero'] == NULL) {
          $class_factura = "tab_sin_factura";
          $class_abono = "active";
          $panel_abono = "in active show";
          $link_factura = "tab_factura";
          $check_facturas_pendientes= "<h2 style='margin: 0 auto;'>No tienes facturas pendientes</h2>";
        }
        else {
          $class_factura = "active";
          $class_abono = "";
          $panel_factura = "in active show";
          $link_factura = "tab_factura";
          $check_facturas_pendientes= Herramientas::crearListadoFacturas($facturas);
        }


        $_POST['facturas'] = $facturas;

      }
      else {

        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/no_encontrado.html");
        $msg_descripcion = "Su cédula no se encuentra registrada.</br>Intente con otra diferente.";

     }

     /*Obtener conceptos de pago*/

     $sql = 'exec sp_ap_obtener_conceptos_pago_pse';

     $stmt=$_conexionAuto->prepare($sql);

     $resultado=$stmt->execute();

     $concepto_Cotizador = $stmt->fetchALL(PDO::FETCH_ASSOC);

     $_POST['concepto_Cotizador'] = $concepto_Cotizador;

     foreach ($concepto_Cotizador as $item) {

         $opciones[] = array('descripcion' => $item['descripcion'],
                        'concepto' => $item['concepto']
                      );
     }


     $lista_conceptos = Herramientas::crearSelectFormalist('concepto_pago',
        "Selecciona una opción",
      $opciones, 0);

      $fact = serialize($facturas);

    	/*etiquetas y contenido de la plantilla*/
      Interfaz::asignarToken("imagenes", $img_ruta, $contenido);
    	Interfaz::asignarToken("boton_procesar", ($boton_procesar), $contenido);
    	Interfaz::asignarToken("msg_titulo_datoscli", ($msg_titulo_datoscli), $contenido);
    	Interfaz::asignarToken("msg_titulo_datospago",$msg_titulo_datospago, $contenido);
      Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
      Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
    	Interfaz::asignarToken("cli_nombres", ($cli_nombres), $contenido);
    	Interfaz::asignarToken("cli_apellidos", ($cli_apellidos), $contenido);
    	Interfaz::asignarToken("cli_email", $cli_email, $contenido);
    	Interfaz::asignarToken("cli_telefono", $cli_telefono, $contenido);
      Interfaz::asignarToken("label_cedula", $cedula, $contenido);
      Interfaz::asignarToken("label_nombres", $nombre, $contenido);
    	Interfaz::asignarToken("label_apellidos", 'APELLIDOS', $contenido);
    	Interfaz::asignarToken("label_telefono", $telefono, $contenido);
    	Interfaz::asignarToken("label_email", $correo, $contenido);

      Interfaz::asignarToken("class_factura", $class_factura, $contenido);
      Interfaz::asignarToken("class_abono", $class_abono, $contenido);

      Interfaz::asignarToken("panel_factura", $panel_factura, $contenido);
      Interfaz::asignarToken("panel_abono", $panel_abono, $contenido);
      Interfaz::asignarToken("link_factura", $link_factura, $contenido);



    	Interfaz::asignarToken("campo_concepto", $select_concepto, $contenido);
      Interfaz::asignarToken("label_concepto", 'CONCEPTO', $contenido);
      Interfaz::asignarToken("campos_facturas_pendientes", $check_facturas_pendientes, $contenido);
      Interfaz::asignarToken("lista_conceptos", $lista_conceptos, $contenido);


      Interfaz::asignarToken("label_valor", 'VALOR', $contenido);
      Interfaz::asignarToken("datos_facturas", $fact, $contenido);



    	return $contenido;
    }

    /** abrirFormularioPSE
     * parametro: $datos
     * autor : Stuard Romero - DESARROLLO AUTOPACIFICO
     * descripcion: abre el formulario con informacion del usuario y la informacion de pago.
     **/
    function abrirFormularioPSE($datos) {
    	global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones;

      $_PATH_WEB = "http://172.16.2.33/pse";

      error_reporting(E_ALL);
      ini_set('display_errors', 'On');


      if(!isset($_SESSION))
      {
          session_start();
      }

      //Logo
      $img_ruta = "Includes/Imagenes";

      //TITULO
      $msg_titulo = 'Sistema de Cotizador de Facturas';
      //Descripción
      $msg_descripcion = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";

    	$boton_procesar = "SIGUIENTE";



    	/* titulos*/
    	$msg_titulo_datoscli = '<h1>Informaci�n personal</h1>';
    	$msg_titulo_datospago = '<h1>Concepto de pago</h1>';


      $cedula = $datos['cli_cedula'];
    	/*datos info pago*/


      $items_factura = $datos['factu'];

      $sql_cliente = "SELECT * FROM cliente where cli_id_cedula = '$cedula'";
      $verificar_clientes = $_obj_database->obtenerRegistrosAsociativos($sql_cliente);
       if(empty($verificar_clientes)){
         //Creando arrays para insertar en tablas
         $campos = array("cli_id_cedula", "cli_nombre", "cli_telefono", "cli_email");
         $clientes['tabla'] = "cliente";
         $clientes['cli_id_cedula'] = $datos['cli_cedula'];
         $clientes['cli_nombre'] =  $datos['cli_nombre'];
         $clientes['cli_telefono'] =  $datos['cli_telefono'];
         $clientes['cli_email'] =   $datos['cli_email'];



         $sql = $_obj_database->generarSQLInsertar($clientes, $campos);

         $_obj_database->ejecutarSql($sql);

       }
       else {
         $sql_cliente = "UPDATE cliente SET cli_telefono = '" . $datos['cli_telefono'] . "', cli_email = '" . $datos['cli_email'] . "' WHERE cli_id_cedula = '" . $cedula . "'";
         $sql_cliente;
         $_obj_database->ejecutarSql($sql_cliente);
       }
       /*Check de facturas*/
         if(!empty($items_factura)) {

          //Insertando transacciones

          $total_factura = sizeof($items_factura);

          $descripcion = "";
          $monto_total = 0;
          $iva = 0;


          $i = 0;

          $sql_verificar_factura = "SELECT * FROM factura where fac_estado = 'Pendiente'";
          $verificar_facturas = $_obj_database->obtenerRegistrosAsociativos($sql_verificar_factura);


          foreach ($items_factura as $concepto => $monto) {


            //exit;
            $total_item_factura = sizeof($monto);
            $total_factura += $total_item_factura - 1;
            for($j = 0; $j < $total_item_factura; $j++) {

              $valores_facturas = explode("|", $monto[$j]);      //[0]=monto;[1]=iva;[2]=id

              $agregar_factura = true;
              /*Verificacion Facturas con estado pendiente*/
              foreach ($verificar_facturas as $id_verif => $factu_verif) {
                /*Si posee el mismo id no se agrega*/
                if($valores_facturas[2] == $factu_verif['fac_id']) {
                  $agregar_factura = false;
                  break;
                }

              }

              if($agregar_factura) {
                $monto_total += $valores_facturas[0];
                $iva += $valores_facturas[1];


                ++$i;
                $cont = strlen($descripcion);

                if($cont < 200) {
                  if($i != $total_factura) {
                    $descripcion .= $concepto . ", ";
                  }
                  else {
                    $descripcion .= $concepto;
                  }
                }
                else {
                  $descripcion = "Cotizador Pendientes Autopacifico";
                }


                $concepto_facturas[$i - 1] = $concepto;
                $monto_facturas[$i - 1] = $valores_facturas[0];
                $iva_facturas[$i - 1] = $valores_facturas[1];
                $id_facturas[$i - 1] = $valores_facturas[2];
              }
            }

          }

          /*Validacion Si ya se han realizado Cotizador en otra seccion*/
          if(isset($id_facturas)){

            //Valor neto sin iva
            $neto = $datos['montoFactura'] - $iva;

            $sql_transaccion = "INSERT INTO transaccion (tra_cli_id, tra_concepto, tra_valor, tra_iva, tra_estado, tra_fecha) VALUES(" .$datos['cli_cedula'].", '" . $descripcion ."' , ". $monto_total .", '". $iva ."', 'Pendiente', NOW())";

            $_obj_database->ejecutarSql($sql_transaccion);

            $sql_transaccion = "SELECT * FROM transaccion WHERE tra_cli_id = '" . $datos['cli_cedula'] . "' ORDER BY tra_id DESC LIMIT 1";

            $ultima_transaccion = $_obj_database->obtenerRegistrosAsociativos($sql_transaccion);

            $tra_id = $ultima_transaccion[0]['tra_id'];

            //inserccion en facturas
            $total_factura = count($concepto_facturas);
            for($i = 0;  $i < $total_factura; $i++) {
                $sql_factura = "INSERT INTO factura (fac_id, fac_concepto, fac_monto, fac_iva, fac_tra_id, fac_estado) VALUES(" .$id_facturas[$i].", '" .$concepto_facturas[$i]."', " . $monto_facturas[$i] ." , ". $iva_facturas[$i] .", ". $tra_id .", 'Pendiente')
                ON DUPLICATE KEY UPDATE fac_tra_id=$tra_id, fac_estado='Pendiente'";

               $_obj_database->ejecutarSql($sql_factura);
            }
            //exit;
          }
          else {
            $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/no_encontrado.html");
            $msg_descripcion = "Ya posee uno o varios Cotizador pendientes.";

            Interfaz::asignarToken("imagenes", $img_ruta, $contenido);
            Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);

            return $contenido;
          }
        }
        /*Pago por abono*/

        else if(isset($datos['concepto_pago'])){
           $valores_conceptos = explode("|", $datos['concepto_pago']);

           $descripcion =  $valores_conceptos[0];
           $concepto = $valores_conceptos[1];

           $monto = $datos['conceptoFactura'];
           $sql_transaccion = "INSERT INTO transaccion (tra_cli_id, tra_concepto, tra_valor, tra_estado, tra_fac_concepto, tra_fecha) VALUES(" .$datos['cli_cedula'].", '" . $descripcion ."' , ". $datos['conceptoFactura'] .", 'Pendiente', '". $concepto ."', NOW())";
           $_obj_database->ejecutarSql($sql_transaccion);//exit;
        }


        $sql_transaccion = "SELECT * FROM transaccion WHERE tra_cli_id = '". $datos['cli_cedula'] ."' ORDER BY tra_id DESC LIMIT 1";

        $ultima_transaccion = $_obj_database->obtenerRegistrosAsociativos($sql_transaccion);

        $cedula_transaccion = $ultima_transaccion[0]['tra_cli_id'];

        if((int)$datos['cli_cedula'] == (int)$ultima_transaccion[0]['tra_cli_id']){

          if($datos['conceptoFactura'] == $ultima_transaccion[0]['tra_valor'] || $datos['montoFactura'] == $ultima_transaccion[0]['tra_valor']) {

            //Guardado en Session

            $_SESSION['submit'] = $_POST['submit'];
            $_SESSION['cedula'] = $datos['cli_cedula'];
            $_SESSION['id_transaccion'] = $ultima_transaccion[0]['tra_id'];
            $_SESSION['monto_total'] = $ultima_transaccion[0]['tra_valor'];

            $ticketOfficeID = 7110;
            $monto_pagar = (double)$ultima_transaccion[0]['tra_valor'];
            $iva = (double)$ultima_transaccion[0]['tra_iva'];

            $paymentID = $ultima_transaccion[0]['tra_id'];
            $descripcion = $ultima_transaccion[0]['tra_concepto'];

            /*Descripcion de Cotizador*/
            if( sizeof($datos['factu']) >= 1) {
              $descripcion = "Pago de Factura: " . $descripcion;
            }
            else if($ultima_transaccion[0]['tra_concepto'] != NULL) {
              $descripcion = "Abono: " . $ultima_transaccion[0]['tra_concepto'];
            }
            else {
              $descripcion = "Pago de Facturas Autopacifico";
            }


            $ip_cliente = $_SERVER['REMOTE_ADDR'];
            $email = $datos['cli_email'];
            $serviceCode = 1001;           //Proporcionado por PSE

            $fields = array(
              0 => array('Name' => 'nombre_cliente', 'Value' => $datos['cli_nombre']),
              1 => array('Name' => 'id_cliente', 'Value' => $datos['cli_cedula']),
              2 => array('Name' => 'telefono_cliente', 'Value' => $datos['cli_telefono'])
            );

            $url = $_PATH_WEB . "index.php?m=Cotizador&accion=reciboPSE";

            $ipCliente = Cotizador::obtenerIpCliente();

            $param=array('ticketOfficeID'=>$ticketOfficeID, 'amount' => $monto_pagar,
                'vatAmount' => $iva, 'paymentID' => $paymentID,
                'paymentDescription' => $descripcion,
                'referenceNumber1' => $ipCliente, 'referenceNumber2' => 'CC',
                'referenceNumber3' => $_SESSION['cedula'],
                'serviceCode' => $serviceCode,
                'email' => $email, 'fields' => $fields,
                'entity_url' => $url
            );


            $resultado_transaccion = NuSoap::crearTransaccionACH($param);


            if($resultado_transaccion['createTransactionPaymentHostingResult']['ReturnCode'] == "OK"){
              $url_lista_bancos = "https://www.pseCotizador.co/PSEHostingUI/GetBankListWS.aspx?enc=" . $resultado_transaccion['createTransactionPaymentHostingResult']['PaymentIdentifier'];
              header("Location: $url_lista_bancos", true, 301);
            }

            //exit;
          }

        }





    	/*etiquetas y contenido de la plantilla*/
      Interfaz::asignarToken("imagenes", $img_ruta, $contenido);
    	Interfaz::asignarToken("boton_procesar", ($boton_procesar), $contenido);
    	Interfaz::asignarToken("msg_titulo_datoscli", $msg_titulo_datoscli, $contenido);
    	Interfaz::asignarToken("msg_titulo_datospago", $msg_titulo_datospago, $contenido);
      Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
      Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
      Interfaz::asignarToken("cli_cedula", $cedula, $contenido);
      Interfaz::asignarToken("cli_nombres", $cli_nombre, $contenido);
    	Interfaz::asignarToken("cli_apellidos", $cli_apellidos, $contenido);
    	Interfaz::asignarToken("cli_email", $cli_email, $contenido);
    	Interfaz::asignarToken("cli_telefono", $cli_telefono, $contenido);
      Interfaz::asignarToken("label_id_tra", $_SESSION['id_transaccion'], $contenido);
    	Interfaz::asignarToken("label_nombres", $datos['cli_nombre'], $contenido);
    	Interfaz::asignarToken("label_apellidos", 'APELLIDOS', $contenido);
    	Interfaz::asignarToken("label_telefono", 'TELÉFONO', $contenido);
    	Interfaz::asignarToken("label_email", 'EMAIL', $contenido);
      Interfaz::asignarToken("label_total", $_SESSION['monto_total'], $contenido);
      Interfaz::asignarToken("label_iva", $iva, $contenido);

      Interfaz::asignarToken("label_concepto", 'CONCEPTO', $contenido);
      Interfaz::asignarToken("label_natural", 'NATURAL', $contenido);
      Interfaz::asignarToken("label_juridica", 'JURIDICA', $contenido);
      Interfaz::asignarToken("label_valor", 'VALOR', $contenido);


    	return $contenido;
    }


     /** obtenerIpCliente
     * parametro:
     * autor : Stuard Romero - DESARROLLO AUTOPACIFICO
     * descripcion: Obtiene la ip del cliente.
     **/
     function obtenerIpCliente() {
        $ipaddress = '';
        if ($_SERVER['HTTP_CLIENT_IP'])
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if($_SERVER['HTTP_X_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if($_SERVER['HTTP_X_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if($_SERVER['HTTP_FORWARDED_FOR'])
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if($_SERVER['HTTP_FORWARDED'])
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if($_SERVER['REMOTE_ADDR'])
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

    /** reciboPSE
     * parametro: $datos
     * autor : Stuard Romero - DESARROLLO AUTOPACIFICO
     * descripcion: abre el formulario con informacion del usuario y la informacion de pago.
     **/
    function reciboPSE($datos) {
    	global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones;

      //Logo
      $img_ruta = "Includes/Imagenes";

      //TITULO
      $msg_titulo = 'Comprobante de TRANSACCIÓN';
      //Descripción
      $msg_descripcion = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";
    	//$boton_login = "iniciar-sesion.jpg";
    	$boton_procesar = "IMPRIMIR";



    	/* titulos*/
    	$msg_titulo_datoscli = '<h1>Informaci�n personal</h1>';
    	$msg_titulo_datospago = '<h1>Concepto de pago</h1>';



      $resultado_consulta = NuSoap::obtenerInfoTransacciones($datos['PaymentID']);

      //Llamada exitosa
      if($resultado_consulta['getTransactionInformationHostingResult']['ReturnCode'] == 'OK') {
        echo "Llamada exitosa a PSE</br>";
        //EStado aprobado
        if($resultado_consulta['getTransactionInformationHostingResult']['State'] == 'OK') {
          $sql_transaccion = "SELECT * FROM transaccion where tra_id = '". $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
          $transaccion = $_obj_database->obtenerRegistrosAsociativos($sql_transaccion);
          //Validacion interna de la deuda
          if($datos['Amount'] == $transaccion[0]['tra_valor']) {
            //Actualizacion de facturas
            if($transaccion[0]['tra_fac_concepto'] == NULL){
              $estado_str = "Aprobado";
              //Actualizar tabla transaccion
              $sql_transaccion = "UPDATE transaccion SET tra_banco_cod = '". $resultado_consulta['getTransactionInformationHostingResult']['BankCode'] ."', tra_banco = '". $resultado_consulta['getTransactionInformationHostingResult']['BankName'] . "', tra_estado = '".$estado_str."', tra_TrazabilityCode = '". $resultado_consulta['getTransactionInformationHostingResult']['TrazabilityCode'] ."' WHERE tra_id = '" . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
              $_obj_database->ejecutarSql($sql_transaccion);

              //Actualizar Factura
              $sql_factura = "UPDATE factura SET fac_estado = '".$estado_str."' WHERE fac_tra_id = '" . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
              $_obj_database->ejecutarSql($sql_factura);
            }
            //Actualizacion de conceptos
            else {
              $estado_str = "Aprobado";
              echo "entro";

              $sql_transaccion = "UPDATE transaccion SET tra_banco_cod = '". $resultado_consulta['getTransactionInformationHostingResult']['BankCode'] ."', tra_banco = '". $resultado_consulta['getTransactionInformationHostingResult']['BankName'] . "', tra_estado = '".$estado_str."', tra_TrazabilityCode = '". $resultado_consulta['getTransactionInformationHostingResult']['TrazabilityCode'] ."' WHERE tra_id = '" . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
              $_obj_database->ejecutarSql($sql_transaccion);
            }

            Cotizador::actualizarServerAutopacifico($resultado_consulta['getTransactionInformationHostingResult']['PaymentID']);
            $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_infoCotizadorRecibo.html");
          }
          //No se aprobo
          else {
            $msg_descripcion = "Ha ocurrido un error, Por favor comuniquese con la empresa";
            Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
            //exit;
          }
        }
        else if ($resultado_consulta['getTransactionInformationHostingResult']['State'] == 'PENDING') {
          $msg_descripcion = "Su Transaccion se encuentra pendiente";
          Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
          $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/vp_error.html");
        }
        else if ($resultado_consulta['getTransactionInformationHostingResult']['State'] == 'FAILED') {
          $msg_descripcion = "Su Transaccion fue fallida";
          Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
          $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/vp_error.html");
        }
        else if ($resultado_consulta['getTransactionInformationHostingResult']['State'] == 'NOT_AUTHORIZED') {
          $msg_descripcion = "Su Transaccion fue rechazada";
          Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
          $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/vp_error.html");

          $sql_transaccion = "SELECT * FROM transaccion where tra_id = '". $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
          $transaccion = $_obj_database->obtenerRegistrosAsociativos($sql_transaccion);
          //Validacion interna de la deuda

            //Actualizacion de facturas
            if($transaccion[0]['tra_fac_concepto'] == NULL){
              $estado_str = "Rechazado";
              //Actualizar tabla transaccion
              $sql_transaccion = "UPDATE transaccion SET tra_banco_cod = '". $resultado_consulta['getTransactionInformationHostingResult']['BankCode'] ."', tra_banco = '". $resultado_consulta['getTransactionInformationHostingResult']['BankName'] . "', tra_estado = '".$estado_str."' WHERE tra_id = '" . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
              $_obj_database->ejecutarSql($sql_transaccion);

              //Actualizar Factura
              $sql_factura = "UPDATE factura SET fac_estado = '".$estado_str."' WHERE fac_tra_id = '" . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
              $_obj_database->ejecutarSql($sql_factura);
            }
            //Actualizacion de conceptos
            else {
              $estado_str = "Rechazado";

              $sql_transaccion = "UPDATE transaccion SET tra_banco_cod = '". $resultado_consulta['getTransactionInformationHostingResult']['BankCode'] ."', tra_banco = '". $resultado_consulta['getTransactionInformationHostingResult']['BankName'] . "', tra_estado = '".$estado_str."' WHERE tra_id = '" . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
              $_obj_database->ejecutarSql($sql_transaccion);
            }
        }
      }
      else if($resultado_consulta['getTransactionInformationHostingResult']['ReturnCode'] == 'ERRORS') {
        echo $resultado_consulta['getTransactionInformationHostingResult']['ErrorMessage'];
        //exit;
      }
      //Error Invalidticketorpassword o paymentid
      else {
        $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/vp_error.html");
        $msg_descripcion = "Ha ocurrido un error, Por favor comuniquese con la empresa";
        Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
      }
      $sql_cliente = "SELECT * FROM cliente where cli_id_cedula = '". $transaccion[0]['tra_cli_id'] ."'";

      //obtener id

      $ultimo_cliente = $_obj_database->obtenerRegistrosAsociativos($sql_cliente);

      $referencia = $resultado_consulta['getTransactionInformationHostingResult']['paymentID'];
      $fecha = $resultado_consulta['getTransactionInformationHostingResult']['SolicitedDate'];


    	/*etiquetas y contenido de la plantilla*/
      Interfaz::asignarToken("label_fecha", $fecha, $contenido);
      Interfaz::asignarToken("label_referencia", $referencia, $contenido);

      Interfaz::asignarToken("label_nombres", $ultimo_cliente[0]['cli_nombre'], $contenido);
      Interfaz::asignarToken("label_banco", $resultado_consulta['getTransactionInformationHostingResult']['BankName'], $contenido);
      Interfaz::asignarToken("label_transaccion", $transaccion[0]['tra_concepto'], $contenido);
      Interfaz::asignarToken("label_estado", $estado_str, $contenido);
      Interfaz::asignarToken("label_ip", '192.168.0.1', $contenido);
      Interfaz::asignarToken("label_valor", $resultado_consulta['getTransactionInformationHostingResult']['Amount'], $contenido);


      Interfaz::asignarToken("imagenes", $img_ruta, $contenido);
    	Interfaz::asignarToken("boton_procesar", ($boton_procesar), $contenido);
    	Interfaz::asignarToken("msg_titulo_datoscli", $msg_titulo_datoscli, $contenido);
    	Interfaz::asignarToken("msg_titulo_datospago", $msg_titulo_datospago, $contenido);
      Interfaz::asignarToken("label_msg_inicio", $msg_titulo, $contenido);
      Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
      Interfaz::asignarToken("cli_cedula", $cedula, $contenido);

    	Interfaz::asignarToken("cli_apellidos", $cli_apellidos, $contenido);
    	Interfaz::asignarToken("cli_email", $cli_email, $contenido);
    	Interfaz::asignarToken("cli_telefono", $cli_telefono, $contenido);
      Interfaz::asignarToken("label_id_tra", $_SESSION['id_transaccion'], $contenido);

    	Interfaz::asignarToken("label_apellidos", 'APELLIDOS', $contenido);

    	Interfaz::asignarToken("label_email", 'EMAIL', $contenido);
      Interfaz::asignarToken("label_total", $datos['montoFactura'], $contenido);
      Interfaz::asignarToken("label_concepto", 'CONCEPTO', $contenido);
      Interfaz::asignarToken("label_natural", 'NATURAL', $contenido);
      Interfaz::asignarToken("label_juridica", 'JURIDICA', $contenido);
      Interfaz::asignarToken("label_valor", 'VALOR', $contenido);

    	//Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);

    	return $contenido;
    }

    /** comprobarTransacciones
     * parametro:
     * autor : Stuard Romero - DESARROLLO AUTOPACIFICO
     * descripcion: Comprueba transacciones que esten pendiente en base de datos.
     **/
    function comprobarTransacciones() {
    	global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones;

      $img_ruta = "Includes/Imagenes";

      //TITULO
      $msg_titulo = 'Comprobante de TRANSACCIÓN';
      //Descripción
      $msg_descripcion = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";
    	//$boton_login = "iniciar-sesion.jpg";
    	$boton_procesar = "IMPRIMIR";



      $sql_transaccion = "SELECT * FROM transaccion WHERE tra_estado = 'Pendiente'";
      $transacciones_pendientes = $_obj_database->obtenerRegistrosAsociativos($sql_transaccion);

      echo "Transacciones pendientes: <pre>";
      var_dump($transacciones_pendientes);

      foreach ($transacciones_pendientes as $tran) {
        //print_r($tran);echo "</br>";
        $resultado_consulta = NuSoap::obtenerInfoTransacciones($tran['tra_id']);
        print_r($resultado_consulta);echo "</br>";
        //Llamada exitosa
        if($resultado_consulta['getTransactionInformationHostingResult']['ReturnCode'] == 'OK') {
          echo "El llamado al servicio fue exitoso</br>";
          //EStado aprobado (estado final)
          if($resultado_consulta['getTransactionInformationHostingResult']['State'] == 'OK') {
            $sql_transaccion = "SELECT * FROM transaccion where tra_id = '". $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
            $transaccion = $_obj_database->obtenerRegistrosAsociativos($sql_transaccion);
            //Validacion interna de la deuda

              //Actualizacion de facturas
              if($tran['tra_fac_concepto'] == NULL){
                $estado_str = "Aprobado";
                //Actualizar tabla transaccion
                $sql_transaccion = "UPDATE transaccion SET tra_banco_cod = '". $resultado_consulta['getTransactionInformationHostingResult']['BankCode'] ."', tra_banco = '". $resultado_consulta['getTransactionInformationHostingResult']['BankName'] . "', tra_estado = '".$estado_str."', tra_TrazabilityCode = '". $resultado_consulta['getTransactionInformationHostingResult']['TrazabilityCode'] ."' WHERE tra_id = '" . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
                $_obj_database->ejecutarSql($sql_transaccion);

                //Actualizar Factura
                $sql_factura = "UPDATE factura SET fac_estado = '".$estado_str."' WHERE fac_tra_id = '" . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
                $_obj_database->ejecutarSql($sql_factura);
              }
              //Actualizacion de conceptos
              else {
                $estado_str = "Aprobado";

                $sql_transaccion = "UPDATE transaccion SET tra_banco_cod = '". $resultado_consulta['getTransactionInformationHostingResult']['BankCode'] ."', tra_banco = '". $resultado_consulta['getTransactionInformationHostingResult']['BankName'] . "', tra_estado = '".$estado_str."', tra_TrazabilityCode = '". $resultado_consulta['getTransactionInformationHostingResult']['TrazabilityCode'] ."' WHERE tra_id = '" . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
                $_obj_database->ejecutarSql($sql_transaccion);
              }

              Cotizador::actualizarServerAutopacifico($resultado_consulta['getTransactionInformationHostingResult']['PaymentID']);
              //$contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/fm_infoCotizadorRecibo.html");

            //No se aprobo

          } //Estado aprobado

          else if ($resultado_consulta['getTransactionInformationHostingResult']['State'] == 'PENDING') {
            //echo "id: " . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "</br>";
            $msg_descripcion = "Su Transaccion se encuentra pendiente";
            echo "</br>";
            Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
            $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/vp_error.html");
          }
          else if ($resultado_consulta['getTransactionInformationHostingResult']['State'] == 'FAILED') {
            //echo "id: " . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "</br>";
            echo $msg_descripcion = "Su Transaccion fue fallida";


            if($tran['tra_fac_concepto'] == NULL){
              $estado_str = "FAILED";
              //Actualizar tabla transaccion
              $sql_transaccion = "UPDATE transaccion SET tra_banco_cod = '". $resultado_consulta['getTransactionInformationHostingResult']['BankCode'] ."', tra_banco = '". $resultado_consulta['getTransactionInformationHostingResult']['BankName'] . "', tra_estado = '".$estado_str."' WHERE tra_id = '" . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
              $_obj_database->ejecutarSql($sql_transaccion);

              //Actualizar Factura
              $sql_factura = "UPDATE factura SET fac_estado = '".$estado_str."' WHERE fac_tra_id = '" . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
              $_obj_database->ejecutarSql($sql_factura);
            }
            //Actualizacion de conceptos
            else {
              $estado_str = "FAILED";

              $sql_transaccion = "UPDATE transaccion SET tra_banco_cod = '". $resultado_consulta['getTransactionInformationHostingResult']['BankCode'] ."', tra_banco = '". $resultado_consulta['getTransactionInformationHostingResult']['BankName'] . "', tra_estado = '".$estado_str."' WHERE tra_id = '" . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
              $_obj_database->ejecutarSql($sql_transaccion);
            }

            Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);
            $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/vp_error.html");
          }
          /*Estado NOT_AUTHORIZED (Estado final)*/
          else if ($resultado_consulta['getTransactionInformationHostingResult']['State'] == 'NOT_AUTHORIZED') {
            echo "id: " . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "</br>";
            //echo $msg_descripcion = "Su Transaccion fue rechazada";
            echo "</br>";
            Interfaz::asignarToken("msg_descripcion", $msg_descripcion, $contenido);

            //Validacion interna de la deuda

              //Actualizacion de facturas
              if($tran['tra_fac_concepto'] == NULL){
                $estado_str = "Rechazado";
                //Actualizar tabla transaccion
                $sql_transaccion = "UPDATE transaccion SET tra_banco_cod = '". $resultado_consulta['getTransactionInformationHostingResult']['BankCode'] ."', tra_banco = '". $resultado_consulta['getTransactionInformationHostingResult']['BankName'] . "', tra_estado = '".$estado_str."' WHERE tra_id = '" . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
                $_obj_database->ejecutarSql($sql_transaccion);

                //Actualizar Factura
                $sql_factura = "UPDATE factura SET fac_estado = '".$estado_str."' WHERE fac_tra_id = '" . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
                $_obj_database->ejecutarSql($sql_factura);
              }
              //Actualizacion de conceptos
              else {
                $estado_str = "Rechazado";

                $sql_transaccion = "UPDATE transaccion SET tra_banco_cod = '". $resultado_consulta['getTransactionInformationHostingResult']['BankCode'] ."', tra_banco = '". $resultado_consulta['getTransactionInformationHostingResult']['BankName'] . "', tra_estado = '".$estado_str."' WHERE tra_id = '" . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
                $_obj_database->ejecutarSql($sql_transaccion);
              }

              Cotizador::actualizarServerAutopacifico($resultado_consulta['getTransactionInformationHostingResult']['PaymentID']);
          }
        }
        else if($resultado_consulta['getTransactionInformationHostingResult']['ReturnCode'] == 'ERRORS') {
          echo $resultado_consulta['getTransactionInformationHostingResult']['ErrorMessage'];
          //exit;
        }
        //Error Invalidticketorpassword o paymentid
        else {
          $contenido = Archivos::obtenerContenidoArchivo($this->_plantillas . "/vp_error.html");

          $estado_str = "INVALIDPAYMENTID";

          $sql_transaccion = "UPDATE transaccion SET tra_estado = '".$estado_str."' WHERE tra_id = '" . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
          $_obj_database->ejecutarSql($sql_transaccion);

          //Actualizar Factura
          $sql_factura = "UPDATE factura SET fac_estado = '".$estado_str."' WHERE fac_tra_id = '" . $resultado_consulta['getTransactionInformationHostingResult']['PaymentID'] . "'";
          $_obj_database->ejecutarSql($sql_factura);


        }

      }
      exit;

    	//Interfaz::asignarToken("mensaje", $datos['mensaje'], $contenido);

    	return $contenido;
    }

    /** actualizarServerAutopacifico
     * parametro: $id_transaccion
     * autor : Stuard Romero - DESARROLLO AUTOPACIFICO
     * descripcion: Actualiza base de datos de Autopacifico a traves de un procedimiento almacenado.
     **/
    function actualizarServerAutopacifico($id_transaccion) {
      global $_obj_database,$_PATH_IMAGENES, $_PATH_WEB, $_opciones, $_conexionAuto;

      //Preparando consulta
      $_conexionAuto = Cotizador::abrirConexionSQL($_host_sql_server, $_db_sql_server, $_user_db_sql_server, $_pass_db_sql_server);    //Abrir conexion SQL Server

      //echo "id de la transaccion $id_transaccion";

      $sql_transaccion = "SELECT * FROM transaccion WHERE tra_id = '". $id_transaccion ."'";

      $ultima_transaccion = $_obj_database->obtenerRegistrosAsociativos($sql_transaccion);
      if($ultima_transaccion[0]['tra_estado'] == "Aprobado"){
        //Facturas
        if($ultima_transaccion[0]['tra_fac_concepto'] == NULL) {
          //echo "pago de factura";
          $codigoBanco = $ultima_transaccion[0]['tra_banco_cod'];
          $nombreBanco = $ultima_transaccion[0]['tra_banco'];
          $sql_factura = "SELECT * FROM factura where fac_tra_id = '". $ultima_transaccion[0]['tra_id'] ."'";

          $facturas = $_obj_database->obtenerRegistrosAsociativos($sql_factura);

          //print_r($facturas);
          foreach ($facturas as $factura) {
            //echo $factura['fac_id'] . "</br>";

            //$sql = 'exec sp_ap_INS_transac_pse_result ?, ?, ?, ?, ?, ?, ?, ?';

            $sql = 'exec sp_ap_INS_transac_pse_result ?, ?, ?, ?, ?, ?, ?, ?, ?';
            //exec sp_ap_INS_transac_pse_result 1006,'BANCO AV VILLAS','',1000000,'ANTICIPO FACTURA TALLER','1002','20181001 20:55:31'

            $stmt=$_conexionAuto->prepare($sql);

            $stmt->bindValue(1, $codigoBanco);
            $stmt->bindValue(2, $nombreBanco);
            $stmt->bindValue(3, $factura['fac_id']);
            $stmt->bindValue(4, $factura['fac_monto']);
            $stmt->bindValue(5, $factura['fac_concepto']);
            $stmt->bindValue(6, 'OK');

            // $datetime is something like: 2014-01-31 13:05:59
            $fecha_bd = strtotime($ultima_transaccion[0]['tra_fecha']);//exit;
            $fecha = date("d/m/y H:i", $fecha_bd);//exit;
            // $myFormatForView is something like: 01/31/14 1:05 PM
            echo "</br>fecha: " . $ultima_transaccion[0]['tra_fecha'];
            $stmt->bindValue(7, $ultima_transaccion[0]['tra_fecha']);

            $stmt->bindValue(8, $ultima_transaccion[0]['tra_cli_id']);

            echo "TrazabilityCode a actualizar: " . $ultima_transaccion[0]['tra_TrazabilityCode'];

            $stmt->bindValue(9, $ultima_transaccion[0]['tra_TrazabilityCode']);
            //var_dump($fecha);
            //exit;
            //var_dump($stmt);
            $resultado=$stmt->execute();
            echo "Resultado de la factura: " . var_dump($resultado);
          }
          //exit;


        }
        //Conceptos
        else {
          $sql = 'exec sp_ap_INS_transac_pse_result ?, ?, ?, ?, ?, ?, ?, ?, ?';
          //exec sp_ap_INS_transac_pse_result 1006,'BANCO AV VILLAS','',1000000,'ANTICIPO FACTURA TALLER','1002','20181001 20:55:31'

          $stmt=$_conexionAuto->prepare($sql);

          $stmt->bindValue(1, $ultima_transaccion[0]['tra_banco_cod']);
          $stmt->bindValue(2, $ultima_transaccion[0]['tra_banco']);
          $stmt->bindValue(3, $ultima_transaccion[0]['tra_fac_concepto']);
          $stmt->bindValue(4, $ultima_transaccion[0]['tra_valor']);
          $stmt->bindValue(5, $ultima_transaccion[0]['tra_concepto']);

          $stmt->bindValue(6, 'OK');

          // $datetime is something like: 2014-01-31 13:05:59
          $fecha_bd = strtotime($ultima_transaccion[0]['tra_fecha']);//exit;
          $fecha = date("d/m/y H:i", $fecha_bd);//exit;
          // $myFormatForView is something like: 01/31/14 1:05 PM
          echo "</br>fecha: " . $ultima_transaccion[0]['tra_fecha'];
          $stmt->bindValue(7, $ultima_transaccion[0]['tra_fecha']);

          $stmt->bindValue(8, $ultima_transaccion[0]['tra_cli_id']);

          $stmt->bindValue(9, $ultima_transaccion[0]['tra_TrazabilityCode']);
          //var_dump($stmt);

          $resultado=$stmt->execute();
          echo "resultado: ";var_dump($resultado);
          //exit;
        }
      }

      //State: NOT_AUTHORIZED
      else if($ultima_transaccion[0]['tra_estado'] == "Rechazado"){
          //Facturas
          if($ultima_transaccion[0]['tra_fac_concepto'] == NULL) {
            //echo "pago de factura";
            $codigoBanco = $ultima_transaccion[0]['tra_banco_cod'];
            $nombreBanco = $ultima_transaccion[0]['tra_banco'];
            $sql_factura = "SELECT * FROM factura where fac_tra_id = '". $ultima_transaccion[0]['tra_id'] ."'";

            $facturas = $_obj_database->obtenerRegistrosAsociativos($sql_factura);

            //print_r($facturas);
            foreach ($facturas as $factura) {
              //echo $factura['fac_id'] . "</br>";

              $sql = 'exec sp_ap_INS_transac_pse_result ?, ?, ?, ?, ?, ?, ?, ?, ?';
              //exec sp_ap_INS_transac_pse_result 1006,'BANCO AV VILLAS','',1000000,'ANTICIPO FACTURA TALLER','1002','20181001 20:55:31'

              $stmt=$_conexionAuto->prepare($sql);

              $stmt->bindValue(1, $codigoBanco);
              $stmt->bindValue(2, $nombreBanco);
              $stmt->bindValue(3, $factura['fac_id']);
              $stmt->bindValue(4, $factura['fac_monto']);
              $stmt->bindValue(5, $factura['fac_concepto']);
              $stmt->bindValue(6, 'NOT_AUTHORIZED');

              // $datetime is something like: 2014-01-31 13:05:59

              // $myFormatForView is something like: 01/31/14 1:05 PM
              echo "</br>fecha: " . $ultima_transaccion[0]['tra_fecha'];
              $stmt->bindValue(7, $ultima_transaccion[0]['tra_fecha']);

              $stmt->bindValue(8, $ultima_transaccion[0]['tra_cli_id']);

              $stmt->bindValue(9, NULL);
              //var_dump($fecha);
              //exit;
              //var_dump($stmt);
              $resultado=$stmt->execute();
              echo "Resultado de ejecucion factura ";
              var_dump($resultado);
            }
            //exit;


          }
          //Conceptos
          else {
            $sql = 'exec sp_ap_INS_transac_pse_result ?, ?, ?, ?, ?, ?, ?, ?, ?';
            //exec sp_ap_INS_transac_pse_result 1006,'BANCO AV VILLAS','',1000000,'ANTICIPO FACTURA TALLER','1002','20181001 20:55:31'

            $stmt=$_conexionAuto->prepare($sql);

            $stmt->bindValue(1, $ultima_transaccion[0]['tra_banco_cod']);
            $stmt->bindValue(2, $ultima_transaccion[0]['tra_banco']);
            $stmt->bindValue(3, $ultima_transaccion[0]['tra_fac_concepto']);
            $stmt->bindValue(4, $ultima_transaccion[0]['tra_valor']);
            $stmt->bindValue(5, $ultima_transaccion[0]['tra_concepto']);

            $stmt->bindValue(6, 'NOT_AUTHORIZED');

            // $datetime is something like: 2014-01-31 13:05:59

            // $myFormatForView is something like: 01/31/14 1:05 PM
            echo "</br>fecha: " . $ultima_transaccion[0]['tra_fecha'];
            $stmt->bindValue(7, $ultima_transaccion[0]['tra_fecha']);

            $stmt->bindValue(8, $ultima_transaccion[0]['tra_cli_id']);
            //var_dump($stmt);
            $stmt->bindValue(9, NULL);

            $resultado=$stmt->execute();
            echo "Resultado de ejecucion en concepto ";
            var_dump($resultado);
            //exit;
          }


    }


      return $contenido;
    }


    function validarAcceso($perfiles_autorizados, $fun_id = 0) {
        //captura el perfil del usuario
        $perfil = $_SESSION['tipo_usuario'];

        $usu_id = $_SESSION['usu_id'];

        $funcionalidadActiva = true;
        if ($fun_id != 0 && $_SESSION['tipo_usuario'] != 'R' && $_SESSION['tipo_usuario'] != 'AD') { //Si existe alguna funcionalidad que controle el acceso a esta accion
            $funcionalidadActiva = $this->verificarFuncionalidadActiva($usu_id, $fun_id);  //true el usuario actual tiene activa esta funcionalidad
        }

        if (sizeof($perfiles_autorizados) > 0) {
            //verifica si el perfil se encuentra en los autorizados
            $existe = array_search($perfil, $perfiles_autorizados);

            if (is_numeric($existe) && $_SESSION[autenticado] == 1) {
                //Asigna el perfil correspondiente
                $perfil = $_SESSION['tipo_usuario'];
            } else {
                //elimina el acceso de todos los perfiles
                $perfil = "-";
            }
        }//Fin de if( sizeof($perfiles_autorizados)>0 )

        if (!$funcionalidadActiva && $_SESSION['tipo_usuario'] != 'R' && $_SESSION['tipo_usuario'] != 'AD') {
            $perfil = "-";
        }

        return $perfil;
    }

//Fin de validarAcceso()


//Fin de eliminarEncuestaEstablecimiento




}

//Fin de clase  UsuarioSistema
?>
