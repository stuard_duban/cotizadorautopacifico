<?php
require_once("Config.php");
require_once("Includes/index.php");

//$_obj_database = new database($_host_db,$_user_db,$_password_db,$_db);

$_obj_interfaz = new Interfaz();

$_obj_mensajes = new Mensajes();


$_obj_interfaz->adicionarCSS("style.css");
$_obj_interfaz->adicionarCSS("bootstrap-select.min.css");
$_obj_interfaz->adicionarCSS("font-awesome.min.css");

$_obj_interfaz->adicionarJS("jquery-3.3.1.js");
$_obj_interfaz->adicionarJS("popper.min.js");
$_obj_interfaz->adicionarJS("bootstrap-select.min.js");
$_obj_interfaz->adicionarJS("bootstrap.min.js");



//Si no se definio un modulo toma por defecto usuarios
$datos['m'] = ( isset($datos['m']) ?  $datos['m'] : "Cotizador");

$datos['m'] = strtolower($datos['m']);

//si el parametro no es un directorio valido deja por defecto el de usuario
if( !is_dir($datos['m']) )
{
    $datos['m'] = "Cotizador";
}
require_once($datos['m']."/index.php");

print_r( $_obj_interfaz->crearInterfazGrafica($datos) );

//$_obj_database->desconectar();

?>
