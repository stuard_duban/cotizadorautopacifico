$(function() {
    $('.selectpicker').selectpicker();
  });
  
  $(document).ready(function(){

    function calcularPorcentajeDescuento() {
        $('td[data-total_porcentaje_descuento]').each(function(index, element) {
            let descuento_padre = 0; 
            let items_hijos = $(element).parent().next().find('tbody tr');
            
            let valor_sin_dcto = 0;
            let valor_con_dcto = 0;
            let dcto_en_pesos = 0;

            $(items_hijos).each(function(subindex, subelement) {
                dcto_en_pesos += $(subelement).find('td[data-monto_descuento]').data('monto_descuento');
                valor_sin_dcto += $(subelement).find('td[data-valor_sin_iva]').data('valor_sin_iva');
            });

            valor_con_dcto = valor_sin_dcto - dcto_en_pesos;
            descuento_padre = (1-(valor_con_dcto / valor_sin_dcto)) * 100;

            $(element).data('total_porcentaje_descuento', parseInt(descuento_padre));
            $(element).html(parseInt(descuento_padre)+'%');
        });
    }

    calcularPorcentajeDescuento();

    /** 
     * Validacion de botones siguiente y anterior
    */
    $('#anterior').hide();
    $('#imprimir').hide();

    $('#siguiente').click(function() {
        let $basico_tab = $('#basico-tab');
        let $indispensable_tab = $('#indispensable-tab');
        let $desgate_tab = $('#desgaste-tab');
        let $basico = $('#basico');
        let $indispensable = $('#indispensable');
        let $desgate = $('#desgaste');
        

        if($basico_tab.hasClass('active')){
            $basico_tab.removeClass('active');
            $indispensable_tab.addClass('active show');
            $basico.removeClass('active show');
            $indispensable.addClass('active show');
            $('#anterior').show();
        }
        else{
            if($indispensable_tab.hasClass('active')){
                $indispensable_tab.removeClass('active show');
                $desgate_tab.addClass('active show');
                $indispensable.removeClass('active show');
                $desgate.addClass('active show');
                $('#siguiente').hide();
                $('#imprimir').show();
            }
        }
    });

    $('#anterior').click(function() {
        let $basico_tab = $('#basico-tab');
        let $indispensable_tab = $('#indispensable-tab');
        let $desgate_tab = $('#desgaste-tab');
        let $basico = $('#basico');
        let $indispensable = $('#indispensable');
        let $desgate = $('#desgaste');
        
        if($indispensable_tab.hasClass('active')){
            $indispensable_tab.removeClass('active show');
            $basico_tab.addClass('active show');
            $indispensable.removeClass('active show');
            $basico.addClass('active show');
            $('#anterior').hide();
        }
        else{
            if($desgate_tab.hasClass('active')){
                $desgate_tab.removeClass('active show');
                $indispensable_tab.addClass('active show');
                $desgate.removeClass('active show');
                $indispensable.addClass('active show');
                $('#siguiente').show(); 
                $('#anterior').show();
            }
        }
    });
    
    $('.card-header .btn-link').click(function() {
        let $icon = $(this).find('i');
        if ($icon.hasClass('fa-plus')) {
            $icon.removeClass('fa-plus');
            $icon.addClass('fa-minus');
        } else {
            $icon.removeClass('fa-minus');
            $icon.addClass('fa-plus');
        }
    });

    // Add minus icon for collapse element which is open by default
    $(".collapse.show").each(function(){
        $(this).prev(".card-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
    });
    
    // Toggle plus minus icon on show hide of collapse element
    $(".collapse").on('show.bs.collapse', function(){
        $(this).prev(".card-header").find(".fa").removeClass("fa-plus").addClass("fa-minus");
    }).on('hide.bs.collapse', function(){
        $(this).prev(".card-header").find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });

    //Agregando subtotal y monto de descuentos a fila padre
    $(".titulo-item").each(function(index, element) {
        $(element).find('td:nth-child(3)').html('$'+convertirAPesos($(element).find('td:last-child').data('monto_sin_iva')));
        // $(element).find('td:nth-child(5)').html('$'+convertirAPesos($(element).find('td:last-child').data('descuento')));
        // $(element).find('td:last-child').data('monto_sin_iva');
    });


    $('table').on('click', 'input[type=checkbox]', function (e) {
        let subtotalColumna = $(this).parents('table').find('tfoot tr:first-child th:last-child');
        let descuentoColumna = $(this).parents('table').find('tfoot tr:nth-child(2) th:last-child');
        let subDescuentoColumna = $(this).parents('table').find('tfoot tr:nth-child(3) th:last-child');
        let ivaColumna = $(this).parents('table').find('tfoot tr:nth-child(4) th:last-child');

        $(this).parents('table').find('tfoot .descuento').val('');
        $(this).parents('table').find('tfoot tr:last-child th:last-child').data('total_con_descuento');
        
        let subtotal = subtotalColumna.data('subtotal');

        let montoDescuento = descuentoColumna.data('descuento');
        // let porcentaje_descuento = subtotalColumna.data('total_porcentaje_descuento');
        
        let totalColumna = $(this).parents('table').find('tfoot tr:last-child th:last-child');
        let total = totalColumna.data('total_mantenimiento');

        let iva = ivaColumna.data('iva');
        
        let $fila = $(this).parents('tr');
        if ($(this).prop('checked')) {
            // porcentaje_descuento += subtotalColumna.data('porcentaje_descuento');

            subtotal = subtotal + $fila.find('td:last-child').data('monto_sin_iva');

            montoDescuento = montoDescuento + $fila.find('td[data-descuento]').data('descuento');
            
            total = total + $fila.find('td:last-child').data('monto');

            iva = iva + $fila.find('td[data-sub_monto_iva]').data('sub_monto_iva')

            $fila.removeClass('ocultar-imprimir');
        } else {
            // porcentaje_descuento -= subtotalColumna.data('porcentaje_descuento');

            subtotal = subtotal - $fila.find('td:last-child').data('monto_sin_iva');

            montoDescuento = montoDescuento - $fila.find('td[data-descuento]').data('descuento');

            total = total - $fila.find('td:last-child').data('monto');

            iva = iva - $fila.find('td[data-sub_monto_iva]').data('sub_monto_iva')

            $fila.addClass('ocultar-imprimir');
        }

        // let iva = total - subtotal;
        
        subtotalColumna.replaceWith('<th data-subtotal="'+subtotal+'">$'+convertirAPesos(subtotal)+'</th>');
        descuentoColumna.replaceWith('<th data-descuento="'+montoDescuento+'">$'+convertirAPesos(montoDescuento)+'</th>');
        subDescuentoColumna.replaceWith('<th data-subdescuento="'+(subtotal - montoDescuento)+'">$'+convertirAPesos(subtotal - montoDescuento)+'</th>');

        ivaColumna.replaceWith('<th data-iva="'+iva+'">$'+convertirAPesos(iva)+'</th>');
        totalColumna.replaceWith('<th data-total_mantenimiento="'+total+'">$'+convertirAPesos(total)+'</th>');

        calcularTotalGlobal();
      });
  
      function calcularDescuentosMantenimientos(element) {
  
          let $seccionCalculo = $(element).parents('tfoot');
  
          if ($seccionCalculo.length !== 0) {
              let $seccionSubtotal = $seccionCalculo.find('tr:first-child th:last-child');
              let $seccionTotalMantenimiento = $seccionCalculo.find('tr:last-child th:last-child');
  
              let estaDeshabilitado = $(element).attr('readonly');
              if (typeof estaDeshabilitado !== typeof undefined && estaDeshabilitado !== false) {
                  $(element).val(0);
                  $seccionTotalMantenimiento.removeAttr('data-total_con_descuento');
                  $seccionTotalMantenimiento.html('$'+
                      convertirAPesos(
                          $seccionTotalMantenimiento.data('total_mantenimiento')
                      )
                  );
  
                  return;
              }
  
              let iva = $seccionTotalMantenimiento.data('total_mantenimiento') - $seccionSubtotal.data('subtotal');
  
              if ($(element).val() > 100 || $(element).val() < 0) {
                  return false;
              }
              let subTotalConDescuento = $seccionSubtotal.data('subtotal') - $seccionSubtotal.data('subtotal') * ($(element).val() / 100);
  
              $seccionTotalMantenimiento.data('total_con_descuento', subTotalConDescuento + iva);
  
              $seccionTotalMantenimiento.html('$'+
                  convertirAPesos(
                      $seccionTotalMantenimiento.data('total_con_descuento')
                  )
              );
          }
      }
  
    //   function calcularTotalGlobal() {
    //       let totalGlobal = 0;
    //       let totalIVA = 0;
    //       let totalConIvaGlobal = 0;
    //       $('.descuento[id!=descuentoGlobal]').each( function(index, element) {
    //           let estaDeshabilitado = $(element).attr('readonly');
    //           calcularDescuentosMantenimientos(element);
  
    //           let $seccionCalculo =  $(this).parents('tfoot');
    //           totalGlobal += $seccionCalculo.find('tr:first-child th:last-child').data('subtotal');

    //           totalIVA += $seccionCalculo.find('tr:nth-child(2) th:last-child').data('iva');
  
    //           if (typeof estaDeshabilitado !== typeof undefined && estaDeshabilitado !== false) {
    //               totalConIvaGlobal += $seccionCalculo.find('tr:last-child th:last-child').data('total_mantenimiento');
    //           } else {
    //               totalConIvaGlobal += $seccionCalculo.find('tr:last-child th:last-child').data('total_con_descuento');
    //           }
    //       });
  
    //       $('#tablaTotalGlobal tbody').find('tr:first-child td:last-child').data('total', totalGlobal).html('$'+convertirAPesos(totalGlobal, 0));

    //       $('#tablaTotalGlobal tbody').find('tr:nth-child(2) td:last-child').data('iva', totalIVA).html('$'+convertirAPesos(totalIVA, 0));

    //       $('#tablaTotalGlobal tbody').find('tr:last-child td:last-child').html('$'+convertirAPesos(totalConIvaGlobal, 0));
  
    //       let esDescuentoGlobal = $('#descuentoGlobal').attr('id');
    //       if (typeof esDescuentoGlobal !== typeof undefined && esDescuentoGlobal !== false) {
    //           let iva  = totalConIvaGlobal - totalGlobal;
  
    //           let descuentoGlobal =  totalGlobal - totalGlobal * ($('#descuentoGlobal').val() / 100);
  
    //           $('#tablaTotalGlobal tbody').find('tr:last-child td:last-child').html('$'+convertirAPesos(descuentoGlobal + iva, 0));
    //       }
    //   }

      function calcularTotalGlobal() {
        let subtotal = 0;
        let iva = 0;
        let total = 0;
        $('table.contenedor-precio').each(function(index, element) {
            let $tablaCalculo = $(element).find('tfoot');
            subtotal += $tablaCalculo.find('th[data-subtotal]').data('subtotal');
            iva += $tablaCalculo.find('th[data-iva]').data('iva');
            total += $tablaCalculo.find('th[data-total_mantenimiento]').data('total_mantenimiento');
            // $tablaCalculo.find('th[data-subtotal]').data('subtotal');
        });
        $('#tablaTotalGlobal tbody').find('tr:first-child td:last-child').html('$'+convertirAPesos(subtotal, 0));
        $('#tablaTotalGlobal tbody').find('tr:nth-child(3) td:last-child').html('$'+convertirAPesos(iva, 0));

        $('#tablaTotalGlobal tbody').find('tr:last-child td:last-child').html('$'+convertirAPesos(total, 0));
      }
  
      $('.descuento').change( function() {
          calcularDescuentosMantenimientos(this);
          calcularTotalGlobal();
      });
  
      $('.descuento').click(function() {
          let $mantenimientos = $('.descuento[id!=descuentoGlobal]');
  
          let esDescuentoGlobal = $(this).attr('id');
          if (typeof esDescuentoGlobal !== typeof undefined && esDescuentoGlobal !== false) {
              $mantenimientos.attr('readonly', 'readonly');
              $(this).removeAttr('readonly');
          } else {
              $mantenimientos.removeAttr('readonly');
              $('#descuentoGlobal').attr('readonly', 'readonly');
              $('#descuentoGlobal').val(0);
          }
  
          calcularTotalGlobal()
      });
  
      function convertirAPesos(valor, decimales=0) {
          return parseFloat(valor.toFixed(decimales)).toLocaleString('es-ES');
      }
  });
  