function validarFormaAutenticacion(forma)
{
	 var error=""; // Errores que se presentan

	if(!validarNulo(forma.usu_login.value))
	{
		error += idioma['usuario_usuario'] + "\n";
	}

	if(!validarNulo(forma.usu_clave.value))
	{
		error += idioma['usuario_contra'] + "\n";
	}

	return desplegarResultadoValidacion(error);
}//Fin de validarFormaAutenticacion()

function validarFormaInformacion(forma)
{
	 var error=""; // Errores que se presentan

	if(!validarNulo(forma.inf_nombre.value))
	{
		error += idioma['informacion_nombre'] + "\n";
	}

	if(!validarNulo(forma.inf_correo.value))
	{
	  error += idioma['usuario_correo'] + "\n";
	}

	if(!validarNulo(forma.inf_captcha.value))
	{
	  error += idioma['usuario_captcha'] + "\n";
	}

	return desplegarResultadoValidacion(error);
}//Fin de validarFormaAutenticacion()

function validarFormaClave(forma)
{
	 var error=""; // Errores que se presentan

	if(!validarNulo(forma.usu_login.value))
	{
		error += idioma['usuario_usuario'] + "\n";
	}

	if(!validarNulo(forma.usu_correo.value))
	{
		error += idioma['usuario_correo'] + "\n";
	}

	return desplegarResultadoValidacion(error);
}//Fin de validarFormaAutenticacion()



function validarFormaCambiarDatos(forma)
{
	 var error=""; // Errores que se presentan

	if(!validarNulo(forma.usu_login.value))
	{
		error += idioma['usuario_usuario'] + "\n";
	}

	if( validarNulo(forma.usu_correo.value) )
	{
	   if( !validarMail(forma.usu_correo.value) )
       {
            error += idioma['usuario_correo'] + "\n";
       }
	}

	if( validarNulo(forma.usu_clave.value) || validarNulo(forma.usu_clave_nueva.value) )
    {
    	if(!validarNulo(forma.usu_clave.value))
    	{
    		error += idioma['usuario_contra'] + "\n";
    	}

    	if(!validarNulo(forma.usu_clave_nueva.value))
    	{
    		error += idioma['usuario_contran'] + "\n";
    	}

    	if( forma.usu_clave_nueva.value != forma.usu_clave_nueva_confirmacion.value)
    	{
    		error += idioma['usuario_contradif'] + "\n";
    	}
    }//Fin de if( validarNulo(forma.usu_clave.value) )

	return desplegarResultadoValidacion(error);
}//Fin de validarFormaCambiarClave()



function validarFormaIngresarUsuarioSistema(forma)
{
	 var error=""; // Errores que se presentan

	if(!validarNulo(forma.usu_nombre.value))
	{
		error += idioma['usuario_nombre'] + "\n";
	}

	if(!validarNumeroEntero(forma.usu_cedula.value))
	{
		error += idioma['usuario_ident'] + "\n";
	}



	if( !validarNumeroEntero(forma.usu_id.value) )
	{
		if(!validarNulo(forma.usu_clave.value))
		{
			error += idioma['usuario_contra'] + "\n";
		}
		else
		{
			if( forma.usu_clave.value != forma.usu_clave_confirmacion.value)
			{
				error += idioma['usuario_clavedif'] + "\n";
			}
		}
	}

	if(!validarNulo(forma.usu_per_id.value))
	{
		error += idioma['usuario_tipo'] + "\n";
	}

	return desplegarResultadoValidacion(error);

}//Fin de validarFormaIngresarUsuarioSistema()



function validarFormaRegistro(forma)
{
	 var error=""; // Errores que se presentan

	if(!validarNulo(forma.usu_nombre.value))
	{
		error += idioma['usuario_nombre'] + "\n";
	}

	if(!validarNulo(forma.usu_identificacion.value))
	{
		error += idioma['usuario_ident'] + "\n";
	}

	if(!validarMail(forma.usu_correo.value))
	{
		error += idioma['usuario_correo'] + "\n";
	}

	if(!validarNulo(forma.usu_login.value))
	{
		error += idioma['usuario_usuario'] + "\n";
	}

	if(!validarNulo(forma.usu_clave.value))
	{
		error += idioma['usuario_contra'] + "\n";
	}
	else
	{
		if( forma.usu_clave.value != forma.usu_clave_confirmacion.value)
		{
			error += idioma['usuario_contradif'] + "\n";
		}
	}


	return desplegarResultadoValidacion(error);

}//Fin de validarFormaRegistro()


function validarFormaContacto(forma)
{
	 var error=""; // Errores que se presentan

	if(!validarNulo(forma.con_comentario.value))
	{
		error += idioma['usuario_sugerencia'] + "\n";
		forma.con_comentario.focus();
	}

	if(validarNulo(forma.con_correo.value))
	{
		if(!validarMail(forma.con_correo.value))
		{
				error += idioma['usuario_correovalido'] + "\n";
		}
	}


	return desplegarResultadoValidacion(error);
}//Fin de validarFormaAutenticacion()


function validarFormaSoporte(forma)
{
	 var error=""; // Errores que se presentan

	if(!validarMail(forma.sop_correo.value))
	{
		error += idioma['usuario_responder']+"\n";
		forma.sop_correo.focus();
	}

	if(!validarNulo(forma.sop_comentario.value))
	{
		error += idioma['usuario_comentario']+"\n";
		forma.sop_comentario.focus();
	}

    //MAOH - 25 Sep 2012 - Incidencia 1761
	//Se quita el captcha del formulario
	/*if(!validarNulo(forma.inf_captcha.value))
	{
	  error += idioma['usuario_captcha'] + "\n";
	}*/

	return desplegarResultadoValidacion(error);
}//Fin de validarFormaSoporte()


function validarCorreo(campo)
{
	//si tiene informacion
	if( validarNulo(campo.value) )
	{
		if(!validarMail(campo.value))
		{
			if( !confirm( idioma['correo_invalido'] ) )
			{
				campo.value="";
				campo.focus();
			}
		}
	}//Fin de if( validarNulo(campo.value) )
}//Fin de validarCorreo

function validarFormaEditarUsuarioGestion(forma)
{
	var error=""; // Errores que se presentan
	if(forma.usu_nombre.value=="")
		{
			error += idioma['est_nombre'] + "\n";
		}

	if(forma.usu_login.value=="")
		{
			error += idioma['usu_login'] + "\n";
		}

	if (!isNaN(forma.usu_login.value))
		{
			error += idioma['usuario_numero'] + "\n";
		}

	if(forma.usu_clave.value !="") //Se va a actualizar la clave
		{
			if( forma.usu_clave.value != forma.usu_clave_confirmacion.value)
				{
					error += idioma['usuario_contradif'] + "\n";
				}
		}
	if(!validarMail(forma.usu_correo.value))
		{
			error += idioma['usuario_correo'] + "\n";
		}
	if(forma.usu_per_id.value=="")
		{
			error += idioma['usuario_tipo'] + "\n";
		}
	if(forma.usc_codigo.value == "")
		{
			error += idioma['usuario_categoria'] + "\n";
		}
	/*if( desplegarResultadoValidacion(error) )
    	{
        	location.href="index.php?m=usuarios&accion=editar&usu_clave='"+forma.usu_clave.value+"'&usu_correo="+forma.usu_correo.value+"&usu_login="+forma.usu_login.value+"&usu_per_id="+forma.usu_per_id.value+"&usu_id="+forma.usu_id.value+"&categoria_codigo="+forma.categoria_codigo.value;
    	}   */

    	//pgs - 16/08/2012 - sev valida que almenos se haya seleccionado una encuesta
	var alMenosUnaEncuesta = false;

	var encuestas = document.getElementsByName('encuestas[]');

	for (var i = 0 ; i < encuestas.length ; i++)
		{
			if (encuestas[i].checked)
				{
					alMenosUnaEncuesta = true;
				}
		}

	if (!alMenosUnaEncuesta)
		{
			error += idioma['seleccionar_encuesta'] + "\n";
		}

    return desplegarResultadoValidacion(error);
}

// AMP - 5/10/15 - valida que se digite un texto en el campo busqueda de gestion usuarios

function validarBusquedaGestionUsuarios(){
	if ($("#usu_busqueda").val().length === 0){
		$(".alert").css("display", "block");
	}else{
		$(".alert").css("display", "none");
	}
}


function validarFormaNuevoUsuario(forma)
{
	var error=""; // Errores que se presentan
	if(forma.usu_nombre.value=="")
		{
			error += idioma['est_nombre'] + "\n";
		}

	if( !validarNulo(forma.usu_login.value))
		{
			error += idioma['usuario_usuario'] + "\n";
		}

	if (!isNaN(forma.usu_login.value))
		{
			error += idioma['usuario_numero'] + "\n";
		}

	if( validarNulo(forma.usu_clave.value) )
		{
			if( forma.usu_clave.value != forma.usu_clave_confirmacion.value)
				{
					error += idioma['usuario_contradif'] + "\n";
				}
		}
	else
		{
		   //agregar esto a idiomas por favor
			error += idioma['pass_vacio'] + "\n";
		}

	if(!validarMail(forma.usu_correo.value))
		{
			error += idioma['usuario_correo'] + "\n";
		}

	if( !validarNulo(forma.usu_per_id.value) )
		{
			error += idioma['usuario_tipo'] + "\n";
		}
	if(forma.tipo_u.value == 'C' && forma.usu_per_id.value == 2 && forma.est_id.value == '')//Se esta creando un usuario tipo hotel pero no se ha seleccionado el hotel
		{
			error += idioma['auxiliar_seleccion_hotel'] + "\n";
		}
	if( !validarNulo(forma.usc_codigo.value) )
		{
			error += idioma['usuario_categoria'] + "\n";
		}

         //pgs - 16/08/2012 - sev valida que almenos se haya seleccionado una encuesta
	var alMenosUnaEncuesta = false;

	var encuestas = document.getElementsByName('encuestas[]');

	for (var i = 0 ; i < encuestas.length ; i++)
		{
			if (encuestas[i].checked)
				{
					alMenosUnaEncuesta = true;
				}
		}

	if (!alMenosUnaEncuesta)
		{
			error += idioma['seleccionar_encuesta'] + "\n";
		}






	//Se verifica que por lo menos se haya seleccionado una de las funcionalidades
	var alMenosUnaFuncionalidad = false;

	var funcionalidades = document.getElementsByName('funcionalidad[]');

	for (var i = 0 ; i < funcionalidades.length ; i++)
		{
			if (funcionalidades[i].checked)
				{
					alMenosUnaFuncionalidad = true;
				}
		}

	if (!alMenosUnaFuncionalidad)
		{
			error += idioma['seleccionar_funcionalidad'] + "\n";
		}

	return desplegarResultadoValidacion(error);
}

function seleccionHotelGestionUsuarios(valor)
	{
		 var error=""; // Errores que se presentan

		if( !validarNulo(valor))
		{
			error += idioma['auxiliar_seleccion_hotel'] + "\n";
		}

		if( desplegarResultadoValidacion(error) )
		{
			location.href="index.php?m=usuarios&accion=gestion&est_id="+valor;
		}
	}

function seleccionTipoUsuarioNuevoUsuario()
	{
		//Consultamos si ya previamente se habia seleccionado una cadena o un hotel al igual que la categoria del usuario y anulamos la seleccion

		var cadena = document.getElementsByName('cad_id');
		if (cadena[0] != undefined) //Ya existe el campo de cadena
			{
				document.forms[0].cad_id.value = '';
			}
		var establecimiento = document.getElementsByName('est_id');
		if (establecimiento[0] != undefined) //Ya existe el campo del hotel
			{
				document.forms[0].est_id.value = '';
			}
		var categoria = document.getElementsByName('usc_codigo');
		if (categoria[0] != undefined) //Ya existe el campo de categoria del usuario
			{
				document.forms[0].usc_codigo.value = '';
			}


		document.forms[0].action = "index.php?m=usuarios&accion=fm_ingresar";
		document.forms[0].submit();
	}

function seleccionCadenaNuevoUsuario()
	{
		document.forms[0].action = "index.php?m=usuarios&accion=fm_ingresar";
		document.forms[0].submit();
	}

function seleccionHotelNuevoUsuario()
	{
		document.forms[0].action = "index.php?m=usuarios&accion=fm_ingresar";
		document.forms[0].submit();
	}

function seleccionCategoriaNuevoUsuario()
	{
		document.forms[0].action = "index.php?m=usuarios&accion=fm_ingresar";
		document.forms[0].submit();
	}

function estadoMenu(id_campo,imagen,path_imagenes)
{
	var objeto = document.getElementById(id_campo);

	if( objeto.style.display=="block" || objeto.style.display=="")
	{
		objeto.style.display="none";
		imagen.src = path_imagenes+"categoria_off.png";
	}
	else
	{
		objeto.style.display="block";
		imagen.src = path_imagenes+"categoria_on.png";
	}
}//Fin de estadoMenu()


function copiarFuncionalidades()
	{
		if (document.forms[0].usu_id_funcionalidades.value == '')
			{
				alert(idioma['seleccionar_usuario_funcionalidad']);
			}
		else
			{
				document.forms[0].action = "index.php?m=usuarios&accion=fm_ingresar";
				document.forms[0].submit();
			}
	}

function seleccionTipoUsuarioEditarUsuario()
	{
		document.forms[0].action = "index.php?m=usuarios&accion=fm_editar";
		document.forms[0].submit();
	}

function seleccionHotelEditarUsuario()
	{
		document.forms[0].action = "index.php?m=usuarios&accion=fm_editar";
		document.forms[0].submit();
	}

function copiarFuncionalidadesEditar()
	{
		if (document.forms[0].usu_id_funcionalidades.value == '')
			{
				alert(idioma['seleccionar_usuario_funcionalidad']);
			}
		else
			{
				document.forms[0].action = "index.php?m=usuarios&accion=fm_editar";
				document.forms[0].submit();
			}
	}

function cambiarEstadoFuncionalidad(elemento, funcionalidad)
	{
		var fila_funcionalidad = document.getElementById("funcionalidad_"+funcionalidad);
		if (elemento.checked ==true)
			{
				fila_funcionalidad.className = "funcionalidades_item_checkeado";
			}
		else
			{
				fila_funcionalidad.className = "funcionalidades_item";
			}
	}

//Funciones para desplegar los cuadritos con los textos para activar las opciones del menu (TOOLTIP) --------------------------------------------------------------------------------------------------------
var anchobocadillo='200px' //default tooltip width
var fondobocadillo='transparent'  //tooltip bgcolor
var disappeardelay=100  //tooltip disappear speed onMouseout (in miliseconds)
var salida_vertical="0px" //horizontal offset of tooltip from anchor link
var salida_horizontal="-13px" //horizontal offset of tooltip from anchor link

/////No further editting needed

var ie4=document.all
var ns6=document.getElementById&&!document.all

if (ie4||ns6)
document.write('<div id="division_bocadillo" style="visibility:hidden;width:'+anchobocadillo+';background-color:'+fondobocadillo+'" ></div>')

function obtenerPosOffser(what, offsettype){
var totaloffset=(offsettype=="left")? what.offsetLeft : what.offsetTop;
var parentEl=what.offsetParent;
while (parentEl!=null){
totaloffset=(offsettype=="left")? totaloffset+parentEl.offsetLeft : totaloffset+parentEl.offsetTop;
parentEl=parentEl.offsetParent;
}
return totaloffset;
}


function mostrarOcultar(obj, e, visible, hidden, anchobocadillo){
if (ie4||ns6)
dropmenuobj.style.left=dropmenuobj.style.top=-500
if (anchobocadillo!=""){
dropmenuobj.widthobj=dropmenuobj.style
dropmenuobj.widthobj.width=anchobocadillo
}
if (e.type=="click" && obj.visibility==hidden || e.type=="mouseover")
obj.visibility=visible
else if (e.type=="click")
obj.visibility=hidden
}

function ieCompatibilidadTest(){
return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}

function borrarBordeNavegador(obj, whichedge){
var edgeoffset=(whichedge=="rightedge")? parseInt(salida_horizontal)*-1 : parseInt(salida_vertical)*-1
if (whichedge=="rightedge"){
var windowedge=ie4 && !window.opera? ieCompatibilidadTest().scrollLeft+ieCompatibilidadTest().clientWidth-15 : window.pageXOffset+window.innerWidth-15
dropmenuobj.contentmeasure=dropmenuobj.offsetWidth
if (windowedge-dropmenuobj.x < dropmenuobj.contentmeasure)
edgeoffset=dropmenuobj.contentmeasure-obj.offsetWidth
}
else{
var windowedge=ie4 && !window.opera? ieCompatibilidadTest().scrollTop+ieCompatibilidadTest().clientHeight-15 : window.pageYOffset+window.innerHeight-18
dropmenuobj.contentmeasure=dropmenuobj.offsetHeight
if (windowedge-dropmenuobj.y < dropmenuobj.contentmeasure)
edgeoffset=dropmenuobj.contentmeasure+obj.offsetHeight
}
return edgeoffset
}

function mostrarBocadillo(menucontents, obj, e, anchobocadillo){
if (window.event) event.cancelBubble=true
else if (e.stopPropagation) e.stopPropagation()
clearocultarBocadillo()
dropmenuobj=document.getElementById? document.getElementById("division_bocadillo") : division_bocadillo
dropmenuobj.innerHTML=menucontents

if (ie4||ns6){
mostrarOcultar(dropmenuobj.style, e, "visible", "hidden", anchobocadillo)
dropmenuobj.x=obtenerPosOffser(obj, "left")
dropmenuobj.y=obtenerPosOffser(obj, "top")
dropmenuobj.style.left=dropmenuobj.x-borrarBordeNavegador(obj, "rightedge")+"px"
dropmenuobj.style.top=dropmenuobj.y-borrarBordeNavegador(obj, "bottomedge")+obj.offsetHeight+"px"
}
}

function ocultarBocadillo(e){
if (typeof dropmenuobj!="undefined"){
if (ie4||ns6)
dropmenuobj.style.visibility="hidden"
}
}

function delayocultarBocadillo(){
if (ie4||ns6)
delayhide=setTimeout("ocultarBocadillo()",disappeardelay)
}

function clearocultarBocadillo(){
if (typeof delayhide!="undefined")
clearTimeout(delayhide)
}

//FIN Funciones para desplegar los cuadritos con los textos para activar las opciones del menu --------------------------------------------------------------------------------------------------------



function existeEnArreglo(valor,arreglo)
{
    for(var i=0; i < arreglo.length; i++ )
    {
        if( arreglo[i] == valor )
        {
            return true;
        }//Fin de 
    }//Fin de for(var i=0; i < arreglo.length; i++ )
    return false;
}//Fin de existeEnArreglo()


/**
 * Función que se encargar de confirmar si un usuaio quiere modificar el paquete
 * de un usuario registrado en el sistema
 * DM - 2013-08-05
 **/
function confirmarCambioPaquete(obj_campo)
{
    var val_ant = $('#paquete_id_ant').val();
    
    //si el valor anterior es diferente al actual y no confirma el cambio entonces
    //deja el valor como estaba
    if( val_ant != obj_campo.value && !confirm( idioma['alerta_confirmar_cambio_paquete']) )
    {
        $(obj_campo).val(val_ant);
        return;
    }
        
	//Consulta las funcionalidades del paquete seleccionado
    var enlace = "m=usuarios&accion=obtener_funcinalidades_paquete&paq_id="+obj_campo.value;
    
	$.ajax({
	   type: "POST",
	   url: "index.php",
	   data: enlace,
	   success: function(datos){
        //obtiene la lista de funcionalidades asignada a un paquete
        lista_funcionalidades = JSON.parse(datos);
                                            
        //obtiene el formulario para extraer los campos de funcionalidades                                                
        var forma = document.getElementById("formaEditarUsuarioSistema");
        var size = forma.elements.length;                        
        for( var i=0; i< size; i++)
        {
            if( forma.elements[i].name=="funcionalidad[]" )
            {
                //si existe entre la lista, debe marcarlo como seleccionado
                if( existeEnArreglo(forma.elements[i].value, lista_funcionalidades) )
                {
                    forma.elements[i].checked = true;
                }//Fin de if( $.inArray(forma.elements[i].value, lista_funcionalidades) )
                else
                {
                    forma.elements[i].checked = false;
                }
            }//Fin de if( forma.elements[i].name=="funcionalidad[]" )
            
        }//Fin de for( var i=0; i< size; i++)                	   		
	   }
	 });
}//Fin de confirmarCambioPaquete