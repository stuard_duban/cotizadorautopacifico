$(function() {
    $('.selectpicker').selectpicker();
  });
  $(document).ready(function(){
      $("#descripcionLinea").attr('value', $(this).find('option:first-child').data('tokens'));

      $('#placa').bind('input', function(){
          let placa = $(this).val();
          if (placa.length === 6) {
              data = {
                  placa: placa
              };
  
              $.ajax({
                  url:'index.php?m=Cotizador&accion=buscarPlacas',
                  type:"POST",
                  data:data,
                  success:function(response){
                        let $contenedorLinea = $('#contenedorLinea');
                        let $contenedorAnho = $('#contenedorAnho');

                        $('#contenedorLinea .form-group, #contenedorAnho .form-group').empty();
                        $('#selectLinea .form-group').empty();

                        $('#anho').remove();
                        if (response.trae_registros) {
                            let $input_modelo = $('<input value="'+response.descripcion_linea+'" data-id_linea="'+response.id_modelo+'" name="linea" id="linea" class="form-control" placeholder="'+response.descripcion_linea+'" readonly>')
                            
                            $contenedorLinea.find('.form-group')
                                .append('<label for="linea">LÍNEA</label>')
                                .append($input_modelo);
                            $contenedorLinea.removeClass('d-none');
    
                            let $input_anho = $('<input value="'+response.anho+'" name="anho" id="anho" class="form-control" placeholder="'+response.anho+'" readonly>')
                            
                            $('#linea_descripcion').val(response.descripcion_linea);
                            $contenedorAnho.find('.form-group')
                                .append('<label for="anho">AÑO</label>')
                                .append($input_anho);
                            $contenedorAnho.removeClass('d-none');
                            
                        } else {
                            obtenerModelos();
                            $('#selectLinea').on('change', '#linea', function(){
                                $('#linea_descripcion').val( $(this).find('option:selected').data('descripcion') );
                                
                                let $inputAnho = $('<input name="anho" id="anho" type="hidden" value="'+$(this).find('option:selected').data('anho')+'">');
                                $('#linea_descripcion').after($inputAnho);
                            });
                        }

                        $('#contenedorKilometraje').removeClass('d-none');
                        $('#contenedorTalleres').removeClass('d-none');
                  },
                  dataType:"json"
              });
          } 
      });	

      $("form").submit(function(e){
        let select = $('select option:selected[value=-1]');
        if (parseInt($(select[0]).val()) === -1) {
            if ($(select[0]).parent().attr('id') === 'linea') {
                alert('Debe seleccionar al menos una Línea');
            } else if ($(select[0]).parent().attr('id') === 'kilometraje') {
                alert('Debe seleccionar al menos un Kilometraje');
            }

            e.preventDefault();
        } else {
            let $linea = $('input#linea');
            // let $anhoInput = $('<input id="anho" type="hidden" value="'+$linea.data('id_linea')+'">')
            if ($linea.length > 0) {
                $linea.val($linea.data('id_linea'));
            }
        }
      });

      $('.tab-pane').each(function(index, element) {
        if ($(element).find('table tbody tr').length === 0) {
            $(element).remove();
            // $(element).find('h2').html('No hay mantenimientos disponibles.');
            // $(element).find('.contenedor-tabla').remove();
        }
      });
  
      function obtenerModelos() {
          $.ajax({
                url:'index.php?m=Cotizador&accion=obtenerModelos',
                type:"POST",
                success:function(data){
                    $('#selectLinea .form-group').append($('<label for="linea">LÍNEA</label>'+data.resultado));
                    $('#linea').selectpicker();
                    $('#linea_descripcion').val( $('#selectLinea #linea option:selected').data('descripcion') );                    
                },
                dataType:"json"
          });
      }

  });