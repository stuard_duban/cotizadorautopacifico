<?php 
// Image thumbnail0r (with an "0" :]) 

// mike[@]filespanker.com 
// type: 
// [optional] 
// jpg = JPEG 
// gif = GIF 
// png = PNG 


function mostrarError($error)
{
	echo $error;
}


function redimensionarImagen($datos)
{
	$w =	$datos['ancho'];
	$h =	$datos['alto'];
	$img = 	$datos['imagen'];
	$mode = $datos['forzar_dimensiones'] == 1 ? true : false;
	
	//Verifica que el alto y el ancho sea superior al tama�o indicado
	$res = getimagesize($img);
	$img_alto = $res[1];
	$img_ancho = $res[0];
	
  
  //si no esta esta forzando las medidas entonces asigna estos valores
	if( $img_alto < $h && !$mode )
    {
        $h = $img_alto; 
    }
  
  //si no esta esta forzando las medidas entonces asigna estos valores  
	if( $img_ancho < $w && !$mode)
    {
        $w = $img_ancho; 
    }

	if (!isset($w)) 
	{ 
		$w = 50; 
	} 
	
	if (!isset($h)) 
	{ 
		$h = 50; 
	}	

	SetType($mode, 'integer'); 
	SetType($w, 'integer'); 
	SetType($h, 'integer'); 
	SetType($img, 'string' ); 
	
	// Initialization 
	// Make sure the file exists... 
	if (!file_exists($img)) 
	{ 
		mostrarError("Error: could not find file: $img.");
		return false;
	} 

	// If the user defined a type to use. 
	if (!isset($type)) 
	{ 
		$ext = explode('.', $img); 
		$ext = $ext[count($ext)-1]; 
		switch(strtolower($ext)) 
		{ 
			case 'jpeg' : 
			$type = 'jpg'; 
			break; 
			default : 
			$type = $ext; 
			break; 
		}
	} 
	

    //DM - 2013-02-18
    //Variable para indicar cuales tipos de imagenes pueden contener transparencia
    $acepta_transparencia = false;
    
	// Create the image... 
	switch (strtolower($type)) 
	{
		case 'jpg': 
		  $tmp = @imagecreatefromjpeg($img); 
		break; 
		
		case 'gif': 
		  $tmp = @imagecreatefromgif($img);
          $acepta_transparencia = true; 
		break; 
		
		case 'png': 
		  $tmp = @imagecreatefrompng($img);
          $acepta_transparencia = true; 
		break; 
		
		default: 
		mostrarError('Error: Unrecognized image format.');
		return false;
		break; 
	}//Fin de switch (strtolower($type))
    
    
    //si no se logro cargar la imagen verifica nuevamente para cada uno de los tipos con cual se puede crear
    //por que a veces el tipo de la imagen no corresponde con el de la imagen
    if ( !$tmp )
    {
        $tmp = @imagecreatefromjpeg($img);
        if( !$tmp )
        {
            $tmp = @imagecreatefromgif($img);
            $acepta_transparencia = true;
            if( !$tmp )
            {
                $tmp = @imagecreatefrompng($img);  
            }
        }//Fin de if( $tmp )
    }//Fin de if ( !$tmp )  
	
	if ($tmp) 
	{ 
		// Resize it 
		$ow = imagesx ($tmp); // Original image width 
		$oh = imagesy ($tmp); // Original image height 
	
		if ($mode) 
		{ 
			// Just smash it up to fit the dimensions 
			$nw = $w; 
			$nh = $h; 
		} 
		else 
		{ 
            
            //si la medida es mas ancho que largo toma la reduccion por ancho 
			if ($ow > $oh) 
			{ 
                //asiga el ancho actual de la imagen
                $nw = $ow;
                //si el ancho es superior al limite, asigna el limite coo ancho
                if( $ow > $w)
                {
                    $nw = $w;
                }
				$nh = ceil( unpercent(percent($nw, $ow), $oh) );
            
            //si el nuevo alto es mayor que el alto limite entonces lo modifica el ancho
            //de acuerdo al nuevo alto
            if( $nh > $h)
            {
               $nh = $h;
               $nw = ceil( unpercent(percent($nh, $oh), $ow) );                                      
            }//Fin de if( $nh > $h)
             
			} 
			else if ($oh > $ow) 
			{
                //asiga el alto actual de la imagen
                $nh = $oh;
                //si el alto es superior al limite, asigna el limite como alto
                if( $oh > $h)
                {
                    $nh = $h;
                }  
				$nw = ceil( unpercent(percent($nh, $oh), $ow) );
            
            //si el nuevo ancho es mayor que el ancho limite, entonces debe recalcular
            //el tama�o
            if( $nw > $w )
            {
               $nw = $w;
               //obtiene el nuevo alto a partir del nuevo ancho
               $nh = ceil( unpercent(percent($nw, $ow), $oh) );
            }//Fin de if( $nw > $w ) 
			} 
			else 
			{
                //coloca el ancho y alto actual de la imagen
				$nh = $oh; 
				$nw = $ow;
				//si sobre pasa los limites asigna los nuevos anchos y altos de acuerdo al limite
				//si el nuevo ancho es mayor que el nuevo alto procesa la imagen por el menor valor
				if( $w > $h && $oh > $h)
                {
                    $nh = $h; 
				    $nw = $h;
                }//Fin de if( $w > $h)
                else if( $h > $w && $ow > $w )
                {
                    $nh = $w; 
				    $nw = $w;
                }
				else
                {
                    $nh = $h; 
				    $nw = $w;
                } 
			} 
		}//Fin de if ($mode)  
		
		$out = imagecreatetruecolor($nw, $nh);
  
        //DM - 2013-02-18
        //Indica si la imagen acepta o o transparencia      
        if( $acepta_transparencia )
        {
            imagesavealpha($out, true);
            $background = imagecolorallocatealpha($out, 255, 255, 255, 127);
            imagecolortransparent($out, $background);
            imagealphablending($out, false);
            imagesavealpha($out, true);        
        }//Fin de if( $acepta_transparencia )
        //DM - 2013-02-18 - Fin 

        
        //DM - 2012-11-19
        //Se cambia el metodo para hacer la copia de imagenes porque la anterior pixelaba la imagen 
        //imagecopyresized($out, $tmp, 0, 0, 0, 0, $nw, $nh, $ow, $oh);
        ImageCopyResampled($out, $tmp, 0, 0, 0, 0, $nw, $nh, $ow, $oh);
        
         
		imagedestroy($tmp); 
	} 
	else 
	{ 
		mostrarError('Could not create image resource.');
		return false;
	} 
	

	if ($out) 
	{ 
		switch (strtolower($type)) 
		{ 
			case 'jpg': 
			//header('Content-type: image/jpeg'); 
			imagejpeg($out,$img,100); 
			break; 
			case 'gif': 
			//header('Content-type: image/gif'); 
			imagegif($out,$img,100); 
			break; 
			
			case 'png': 
			//header('Content-type: image/png'); 
			imagepng($out,$img,9); 
			break; 
		} 
		
		imagedestroy($out); 
	} 
	else 
	{ 
		mostrarError('ERROR: Could not create resized image.');
		return false;
	} 

	return true;
}//Fin de redimensionarImagen($datos)

function percent($p, $w) 
{ 
	return (real)(100 * ($p / $w)); 
} 
function unpercent($percent, $whole) 
{ 
	return (real)(($percent * $whole) / 100); 
} 

?>