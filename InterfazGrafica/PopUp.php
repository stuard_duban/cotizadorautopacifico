<?php
/**
 *	Nombre: PopUp.php
 *	Descripci�n: Maneja el formato de una ventana emergente creada como pop-up
 *	Autor: Ra�l Ernesto Villamar�n
 *	E-mail: shadowrev@gmail.com
 *	Fecha de Creaci�n: 24-03-2007
 *	Fecha de �ltima Modificaci�n: 29-02-2008 por Diana Patricia Tasc�n
 */

require_once("../Config.php");

class PopUp
{

/******	CONSTRUCTOR DE LA CLASE	******/
	function PopUp()
	{
	}

/******	CREAR POP UP	******/
/*
 *	Utilidad:
 *		Crea la estructura HTML de una p�gina web
 *	Parametros de entrada:
 *		$contenido_popup:String -=> El contenido que viene dentro del bloque <body>
 *	Valores de Retorno:
 *		void
 */
	function crearPopup($contenido_popup)
	{
		global $_PATH_WEB;

		print "
			<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.1//EN' 'http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd'>
			<html xmlns='http://www.w3.org/1999/xhtml'>
				<head>
					<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1' />
					<link rel='stylesheet' type='text/css' href='".$_PATH_WEB."/InterfazGrafica/css/hoja_estilos.css'>
					<title>Zentinella</title>
				</head>
				<body>
					<div id='popup'>
							$contenido_popup
					</div>
				</body>
			</html>
		";
	}


	/******	CREAR WINCOMBO	******/
	/**
	 *	Utilidad:
	 *		Construir un wincombo para el manejo de  consultas b�sicas en los formularios
	 *	Parametros de entrada:
	 *		$nombre_campo_texto:  nombre del campo donde se va escribir el texto de consulta
	 *		$otros_parametros: arreglo con los nombres de otros par�metros que se necesiten.
	 *		$accion:  direccion URL con la accion que realiza la consulta
	 *		$forma:  con el nombre de la forma
	 *
	 *	Valores de Retorno:
	 *		HTML con el formato para construir el campo especial de consulta
	 */
	function crearWincombo($nombre_campo_texto, $otros_parametros, $accion, $forma)
	{
		// Adici�n de otros par�metros
		$str_otros_parametros = "";
		if(!is_null($otros_parametros) || ($otros_parametros == ""))
		{
			foreach($otros_parametros as $parametro => $value)
			{
				$str_otros_parametros .= "+'&$parametro=$value'";
			}
		}

		$campo_wincombo="
			<input align='left' type='text' name='$nombre_campo_texto' value=''>
			<input type='button' class='boton' value='...' onClick=\"abrirPopUp('$accion&clave='+document.$forma.$nombre_campo_texto.value$str_otros_parametros, 'ventana', 500, 400, 100, 110)\">
		";

		return $campo_wincombo;
	}


	/******	CREAR POP UP WINCOMBO	******/
	/**
	 *	Utilidad:
	 *		Crea el codigo HTML del PopUp
	 *	Parametros de entrada:
	 *		$forma: nombre de la forma
	 *		$titulo_pop_up:  string con el nombre del Pop Up
	 *		$titulos_datos_a_mostrar:  array con los nombres de las columnas del listado del popUp
	 *		$datos: Array con los datos del resultado de la consulta
	 *		$long: tama�o del array de datos
	 *		$campo_texto -> nombre del campo de texto donde se escribir� la opci�n que escoja el usuario
	 *
	 *	Valores de Retorno:
	 *		void
	 */
	function crearPopUpWinCombo($forma, $titulo_pop_up, $titulos_datos_a_mostrar, $datos, $long, $campo_texto)
	{
		global $_PATH_WEB,$_PATH_IMAGENES,$_PATH_LOCAL_BD;

		$contenido_listado = "
		<div id='contenido_listado'>
			<br>
			<h3>$titulo_pop_up</h3>
			";
		if(is_array($datos) && $long>0)
		{
			$contenido_listado .= "
			<table width='100%' border='0' cellpadding='5' cellspacing='0'>
				<tr class=encabezado_tabla>
			";

			foreach($titulos_datos_a_mostrar as $titulo)
			{
				$contenido_listado .= "
					<td align='left' nowrap>
						$titulo
					</td>
				";
			}
			$contenido_listado .= "
				</tr>
			";
			foreach($datos as $dato_fila)
			{
				$contenido_listado .= "
				<tr>";

				$keys = array_keys($dato_fila);
				$dato = $dato_fila[$keys[0]];

				for($i=0; $i<sizeof($dato_fila); $i++)
				{
					$contenido_listado .= "
						<td align='left'>
							<a href='javascript:window.opener.document.$forma.$campo_texto.value=\"".$dato."\";window.close();'>
						".$dato_fila[$keys[$i]]."
							</a>
						</td>";
				}

				$contenido_listado .= "
				</tr>";
			}

			$contenido_listado .= "
				</table>
			";
		} // Fin if(is_array($datos) && $long>0)

		//Si no hay datos obtenidos
		else
		{
			$contenido_listado .= "
						<table width='100%' border='0' cellpadding='5' cellspacing='0'>
							<tr>
								<td align=center>
									<br>
									<h3>No se han encontrado resultados que cumpla con el criterio de b&uacute;squeda.</h3>
									<br><br>
								</td>
							</tr>
						</table>
						";
		}

		$contenido_listado .= "
			<div id='enlace'>
				<br>
				<a href='javascript:self.close()'>Cerrar</a>
			</div>
		</div>
		";
		$this->crearPopup($contenido_listado);
	} // Fin crearPopUpWinCombo()
} // Fin clase PopUp()

?>