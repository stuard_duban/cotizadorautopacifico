<?php
/*
*	Nombre: Mensaje.php
*	Descripci�n: Clase que incluye los mensajes que aparecen en la aplicaci�n
*	Autor: Christian Felipe Benitez Casta�o
*	E-mail:  pipeloco@gmail.com
*	Fecha de Creaci�n: 21-03-2007
*	Fecha de �ltima Modificaci�n: 21-03-2007
*/

class Mensajes
{
	function Mensaje()
	{

	}

	/********************************* CREAR MENSAJE ************************************/
	/*
	* Utilidad:	Se encarga de crear el mensaje de la interfaz
	* Par�metros de entrada:
	*	$id_mensaje = numero que identifica el mensaje a mostrar
	*	$cadena_objeto = cadena que identifica el objeto del que se habla en el mensaje
	*	$tipo_accion_mensaje = Valor con el tipo de la acc�n del mensaje
	*		0 -> Consultar
	*		1 -> Ingresar
	*		2 -> Actualizar
	*		3 -> Eliminar
	* Valores de retorno:
	*	void
	*/
	function crearMensaje($id_mensaje, $cadena_objeto, $tipo_accion_mensaje)
	{
		global $_PATH_WEB;

		switch($tipo_accion_mensaje)
		{
			case 0:
				$accion = "consultado";
				break;

			case 1:
				$accion = "ingresado";
				break;

			case 2:
				$accion = "modificado";
				break;

			case 3:
				$accion = "eliminado";
				break;
		}//Fin switch($tipo_accion_mensaje)

		switch($id_mensaje)
		{
			case 1:
				$imagen = "icon_exito.jpg";
				$mensaje = $cadena_objeto." se ha ".$accion." con &eacute;xito.";
				break;

			case 2:
				$imagen = "icon_error.jpg";
				$mensaje = $cadena_objeto." no ha sido ".$accion.". Debido a que ya existe un elemento igual en la Base de Datos.";
				break;

			case 0:
				$imagen = "icon_error.jpg";
				$mensaje = $cadena_objeto." no ha sido ".$accion.". Debido a un error en la Conexi&oacute;n en la Base De Datos.";
				break;

			case -1:
				$imagen = "icon_error.jpg";
				$mensaje = $cadena_objeto." no ha sido ".$accion.". Debido a que no se ha asignado el nombre de la tabla.";
				break;

			case -2:
				$imagen = "icon_error.jpg";
				$mensaje = $cadena_objeto." no ha sido ".$accion.". Debido a que no se han asignado los atributos de la tabla.";
				break;

			case -3:
				$imagen = "icon_error.jpg";
				$mensaje = $cadena_objeto." no ha sido ".$accion.". Debido a un error en la consulta al ejecutarse en el SMBD";
				break;

			case -4:
				$imagen = "icon_error.jpg";
				$mensaje = "La consulta no arroj&oacute; resultados";
				break;

			case -5:
				$imagen = "icon_error.jpg";
				$mensaje = $cadena_objeto." no ha sido ".$accion.". Debido a que no se pudo construir la instrucci�n SQL";
				break;

			case -6:
				$imagen = "icon_error.jpg";
				$mensaje = $cadena_objeto." no ha sido ".$accion.". Debido a que no se indic&oacute; una condici&oacute;n";
				break;

			case -7:
				$imagen = "icon_advertencia.jpg";
				$mensaje = $cadena_objeto;
				break;

		}//Fin switch($id_mensaje)
		$mensaje_completo = "
		<center><div id='mensaje'>
			<img src='".$_PATH_WEB."/InterfazGrafica/Imagenes/$imagen' /> $mensaje<br><br>
		</div></center><!--Cierra el div del mensaje-->";

		return $mensaje_completo;
	}//Fin function crearMensaaje($id_mensaje, $cadena_objeto, $tipo_accion_mensaje)
}
?>