<?php
/*
*	Nombre: Listado.php
*	Descripci�n: Clase que maneja la interfaz grafica de los listados de la aplicaci�n
*	Autor: Christian Felipe Benitez Casta�o
*	E-mail:  ingeniero.benitez@gmail.com
*	Fecha de Creaci�n: 12-05-2008
*	Fecha de �ltima Modificaci�n: 12-05-2008
*/
class Listado extends Objeto
{
	function Listado()
	{
		parent::Objeto();
	}
	
	/********************************* CREAR LISTADO ************************************/
	/*
	* Utilidad:	Se encarga de crear un listado
	* Par�metros de entrada:
	*	$arreglo_datos['TITULO']: Titulo que va en la interfaz del listado
	*	$arreglo_datos['DESCRIPCION']: Descripcion del listado
	*	$arreglo_datos['CAMPOS']: Arreglo con los nombres de los campos del listado
	*	$arreglo_datos['LISTADO']: Arreglo con los datos del listado
	*	$arreglo_datos['OPCIONES']: Arreglo con las opciones a manejar en el listado
	*	$arreglo_datos['ACCION_LISTAR']: Accion para volver a listar, accion que se ejecuta en el siguiente y atras
	*	$arreglo_datos['ACCION_VER']: Accion para ir al detalle de un objeto
	*	$arreglo_datos['ACCION_COPIAR']: Accion para copiar un objeto
	*	$arreglo_datos['ACCION_ACTUALIZAR']: Accion para ir al formulario para actualizar
	*	$arreglo_datos['ACCION_ELIMINAR']: Accion para eliminar un objeto del listado
	*	$arreglo_datos['ACCION_ARCHIVAR']: Accion para ejecutar la opcion de archivar
	*	$arreglo_datos['ACCION_VOLVER']: Accion para volver a la pantalla de la que se viene
	*	$arreglo_datos['MENSAJE']: Mensaje de exito o error
	*	$arreglo_datos['BANDERA']:
	*	$arreglo_datos['URL_MODIFICAR_COMPLETA']: Si la URL viene completa
	*	$arreglo_datos['TEXTO_VER']
	* Valores de retorno:
	*	void
	*/
	function crearListado($arreglo_datos)
	{
	
		global $_PATH_WEB;
		//Herramientas::i($arreglo_datos);exit;
		echo "
		<center>
		".$arreglo_datos['MENSAJE']."
		<div>";

		if($arreglo_datos['TITULO'] != "")
		{
			echo "
			<h5>".$arreglo_datos['TITULO']."</h5>";
		}
		
		echo "
			<p>".$arreglo_datos['DESCRIPCION']."</p>
		
			<table border='0' cellpadding='0' cellspacing='0' width='100%'>";
		if(count($arreglo_datos['CAMPOS']) > 0)
		{
				// coloca las etiquetas de las columnas en la tabla
				echo "
				<tr class='encabezado_tabla'>";
			foreach($arreglo_datos['CAMPOS'] as $campo => $etiqueta)
			{
				if( $etiqueta!="" )
				{
					echo "
					<td><b>$etiqueta</b></td>";
				}
			}//Fin de foreach()
			if( isset($arreglo_datos['OPCIONES']['ver']) )
			{
					echo "
					<td width=\"60\"><b>Ver</b></td>";
			}
			if( isset($arreglo_datos['OPCIONES']['enviar']) )
			{
					echo "
					<td width=\"60\"><b>Enviar</b></td>";
			}
			if( isset($arreglo_datos['OPCIONES']['copiar']) )
			{
					echo "
					<td width=\"60\"><b>Copiar</b></td>";
			}
			if( isset($arreglo_datos['OPCIONES']['actualizar']) )
			{
					echo "
					<td width=\"60\"><b>Modificar</b></td>";
			}
			if( isset($arreglo_datos['OPCIONES']['eliminar']) )
			{
					echo "
					<td width=\"60\"><b>Eliminar</b></td>";
			}
			if( isset($arreglo_datos['OPCIONES']['archivar']) )
			{
					echo "
					<td id='listado_encabezado' width=\"60\"><b>Archivar</b></td>";
			}
				echo "
				</tr>";
			
      
      
			
			$size=0;
			//cuenta el numero de resultados 
      foreach($arreglo_datos['LISTADO'] as $key=> $value)
			{
        if (is_array($value))
          $size++;
      }
			
			
			if( $size > 0 && is_array($arreglo_datos['LISTADO']))
			{
		
			  $fondo="fondo_claro";
				// para cada resultado obtenido
				for($i = 0; $i < $arreglo_datos['LISTADO']['limite'] && $i < $size; $i++)
				{
					$obj_objeto = $arreglo_datos['LISTADO'][$i];
					
				echo "
				<tr class='$fondo'>";
				
				if ($fondo=="fondo_claro")
          $fondo="fondo_oscuro";
        else
          $fondo="fondo_claro";
            
            
					// coloca los valores de cada objeto
					foreach($arreglo_datos['CAMPOS'] as $campo => $etiqueta)
					{
						if($etiqueta != "")
						{
							eval('$valor = $obj_objeto['.$campo.'];');
							$tamanho_cadena = strlen($valor);
							
							if ($tamanho_cadena > 58)
							{
								$valor = substr($valor, 0, 55);
								$valor .= "...";
							}
					echo "
					<td id='listado'>".$valor."</td>";
						}
					}//Fin de foreach()
					
					// obtiene los atributos del objeto
					$atributos = array_keys($arreglo_datos['CAMPOS']);
					// para colocar el enlace de ver, actualizar y  eliminar
					if( isset($arreglo_datos['OPCIONES']['ver']) )
					{
						$enlace = $arreglo_datos['OPCIONES']['ver'];
						for($j=0; $j<count($atributos); $j++)
						{
							eval('$valor = $obj_objeto['.$atributos[$j].'];');
							$enlace = str_replace("%".$atributos[$j]."%",$valor,$enlace);
						}//Fin de for($i=0; $i<count($atributos); $i++)
					echo "
					<td align=\"center\">
						<a href=\"index.php?accion=".$arreglo_datos['ACCION_VER']."&$enlace\">
							<img alt='".$arreglo_datos['TEXTO_VER']."' title='".$arreglo_datos['TEXTO_VER']."' src='".$_PATH_WEB."/InterfazGrafica/Imagenes/ver.jpg' border=0>
						</a>
					</td>";
					}//Fin de if( isset($opciones['ver']) )
					if( isset($arreglo_datos['OPCIONES']['enviar']) )
					{
						$enlace = $arreglo_datos['OPCIONES']['enviar'];
						for($j=0; $j<count($atributos); $j++)
						{
							eval('$valor = $obj_objeto['.$atributos[$j].'];');
							$enlace = str_replace("%".$atributos[$j]."%",$valor,$enlace);
						}//Fin de for($i=0; $i<count($atributos); $i++)
					echo "
					<td align=\"center\">
						<a href=\"index.php?accion=".$arreglo_datos['ACCION_ENVIAR']."&$enlace\">
							<img src='".$_PATH_WEB."/InterfazGrafica/Imagenes/icon_enviar.jpg' border=0 width=20 height=20>
						</a>
					</td>";
					}//Fin de if( isset($opciones['enviar']) )
					if( isset($arreglo_datos['OPCIONES']['actualizar']) )
					{
						$enlace = $arreglo_datos['OPCIONES']['actualizar'];
						for($j=0; $j<count($atributos); $j++)
						{
							eval('$valor = $obj_objeto['.$atributos[$j].'];');
							$enlace = str_replace("%".$atributos[$j]."%",$valor,$enlace);
						}//Fin de for($i=0; $i<count($atributos); $i++)
						if( isset($arreglo_datos['OPCIONES']['copiar']) )
						{
							$enlace = $arreglo_datos['OPCIONES']['copiar'];
							for($j=0; $j<count($atributos); $j++)
							{
								eval('$valor = $obj_objeto['.$atributos[$j].'];');
								$enlace = str_replace("%".$atributos[$j]."%",$valor,$enlace);
							}//Fin de for($i=0; $i<count($atributos); $i++)
							echo "
							<td align=\"center\">
								<a href=\"index.php?accion=".$arreglo_datos['ACCION_COPIAR']."&$enlace\">
									<img src='".$_PATH_WEB."/InterfazGrafica/Imagenes/icon_editar.jpg' border=0 >
								</a>
							</td>";
						}//Fin de if( isset($opciones['copiar']) )
						if($arreglo_datos['URL_MODIFICAR_COMPLETA'])
						{
							echo "
							<td align=\"center\">
								<a href=\"".$arreglo_datos['ACCION_ACTUALIZAR']."&$enlace\">
									<img src='".$_PATH_WEB."/InterfazGrafica/Imagenes/icon_editar.jpg' border=0>
								</a>
							</td>";
						}
						else
						{
							echo "
							<td align=\"center\">
								<a href=\"index.php?accion=".$arreglo_datos['ACCION_ACTUALIZAR']."&$enlace\">
									<img src='".$_PATH_WEB."/InterfazGrafica/Imagenes/icon_editar.jpg' border=0 width=20 height=20 title=Editar>
								</a>
							</td>";
						}
					}//Fin de if( isset($opciones['actualizar']) )
					if( isset($arreglo_datos['OPCIONES']['eliminar']) )
					{
						$enlace = $arreglo_datos['OPCIONES']['eliminar'];
						for($k=0; $k<count($atributos); $k++)
						{
							eval('$valor = $obj_objeto['.$atributos[$k].'];');
							$enlace = str_replace("%".$atributos[$k]."%",$valor,$enlace);
						}//Fin de for($i=0; $i<count($atributos); $i++)
					echo "
					<td align=\"center\">
						<a href=\"index.php?accion=".$arreglo_datos['ACCION_ELIMINAR']."&$enlace\" onClick=\"return confirm('Est� seguro que desea eliminar?')\">
							<img src='".$_PATH_WEB."/InterfazGrafica/Imagenes/icon_eliminar.jpg' border=0 width=20 height=20 title='Eliminar'>
						</a>
					</td>";
					}//Fin de if( isset($opciones['eliminar']) )
					
					if( isset($arreglo_datos['OPCIONES']['archivar']) )
					{
						$enlace = $arreglo_datos['OPCIONES']['archivar'];
						for($k=0; $k<count($atributos); $k++)
						{
							eval('$valor = $obj_objeto['.$atributos[$k].'];');
							$enlace = str_replace("%".$atributos[$k]."%",$valor,$enlace);
						}//Fin de for($i=0; $i<count($atributos); $i++)
					echo "
					<td align=\"center\">
						<a href=\"index.php?accion=".$arreglo_datos['ACCION_ARCHIVAR']."&$enlace\" onClick=\"return confirm('&iquest; Est� seguro que desea archivar ?\\n\\nAl archivar este registro, se realizar&aacute; una copia de todos los datos relacionados en scripts SQL, para posteriormente ser eliminados de la base de datos.\\n\\nPor &uacute;ltimo, el estado del proyecto cambiar&aacute; a Archivado')\">
							<img src='".$_PATH_WEB."/InterfazGrafica/Imagenes/archivar.jpg' border=0>
						</a>
					</td>";
					}//Fin de if( isset($opciones['archivar']) )
				echo "
				</tr>";
				}//Fin de for($i=0; $i<$this->limite && $i<$size; $i++ )
			}//Fin de if( $size > 0 && is_array($listado) )
			else
			{
				echo "
				<tr>
					<td colspan=".(count($arreglo_datos['CAMPOS'])+2)." align=\"center\">
						<h3>No existen registros </h3>
					</td>
				</tr>";
			}
		}//Fin de if( count($campos)>0 )
		// para la paginacion
		
		if( $arreglo_datos['LISTADO']['offset']> 0 )
		{
			$offset = $arreglo_datos['LISTADO']['offset'] - $arreglo_datos['LISTADO']['limite'];
			$limite = $arreglo_datos['LISTADO']['limite'];
			if($offset < 0)
			{
				$offset = 0;
			}
			
			$atras = "
				<a href=\"index.php?accion=".$arreglo_datos['ACCION_LISTAR']."&limite=".$limite."&offset=".$offset.$arreglo_datos['ENLACE']."\">
					<img src='".$_PATH_WEB."/InterfazGrafica/Imagenes/atras.jpg' border=0></a>";
		}
		
		if( $size > $arreglo_datos['LISTADO']['limite'] && $arreglo_datos['LISTADO']['limite'] != '')
		{
			if($arreglo_datos['LISTADO']['offset'] == 0)
			{
				$siguiente = "
				<a href=\"index.php?accion=".$arreglo_datos['ACCION_LISTAR']."&limite=".($arreglo_datos['LISTADO']['limite'])."&offset=".($arreglo_datos['LISTADO']['offset']+$arreglo_datos['LISTADO']['limite']).$arreglo_datos['ENLACE']."\">
					<img src='".$_PATH_WEB."/InterfazGrafica/Imagenes/siguiente.jpg' border=0></a>";
			}
			else
			{
				$siguiente = "
				<a href=\"index.php?accion=".$arreglo_datos['ACCION_LISTAR']."&limite=".($arreglo_datos['LISTADO']['limite'])."&offset=".($arreglo_datos['LISTADO']['offset']+$arreglo_datos['LISTADO']['limite']).$arreglo_datos['ENLACE']."\">
					<img src='".$_PATH_WEB."/InterfazGrafica/Imagenes/siguiente.jpg' border=0></a>";
			}			
		}//Fin if( $size > $arreglo_datos['LISTADO']['limite'] && $arreglo_datos['LISTADO']['limite'] != '')
		echo "
			</table>
			<div id='enlace'>
				$atras &nbsp; $siguiente<br>";
				/*if($arreglo_datos['BANDERA'] != 0)
				{
					echo "<a href='".$_PATH_WEB."/Proyecto/index.php?accion=".$arreglo_datos['ACCION_VOLVER']."'>Volver</a>";
				}
				else
				{
					echo "<a href=\"index.php?accion=".$arreglo_datos['ACCION_VOLVER']."\">Volver</a>";
				}*/
			echo "
			</div>
		</div><!--Cierra el div del contenido_listado-->";
	}//Fin crearListado()
	
	/********************************* ADICIONAR JS ************************************/
	/*
	* Utilidad:	Se encarga de incluir la ruta de los scripts de las validaciones
	* Par�metros de entrada:
	*	$nombre_script -> Nombre del archivo JavaScript
	* Valores de retorno:
	*	void
	*/
	function adicionarJS($nombre_script)
	{
		$this->_javaScripts[$nombre_script] = "<script language='JavaScript' type='text/JavaScript' src='../SistemaBase/Herramientas/$nombre_script'></script>";
	}//Fin function adicionarJS($nombre_script)
}//Fin class Listado
?>
