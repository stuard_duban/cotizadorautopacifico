<?php
include_once("../Config.php");


function imprimirConsultaTorta($datos, $nombreGrafico, $nombres)
{
	global $_PATH_LOCAL;
	global $_PATH_WEB;

	$grafico = new PieGraph(155,150,"auto");
	//$grafico->SetShadow();

	$grafico->title->Set($nombreGrafico);
	$grafico->title->SetFont(FF_FONT1,FS_BOLD);

	$bplot = new PiePlot3D($datos);
	$bplot->ExplodeSlice(1);
	$bplot->SetCenter(0.45);
	$bplot->SetLegends($nombres);
	$bplot->SetSliceColors(array('#003f7d','#00b95c','#ffff6a', '#006633', '#8cc6ff', '#cb9900'));

	$grafico->Add($bplot);
	$name="$_PATH_LOCAL/TopLlamadas/graficas/grafico_torta_$nombreGrafico.png";
	$grafico->Stroke($name);

	return "<img alt='reporte' src='$_PATH_WEB/TopLlamadas/graficas/grafico_torta_$nombreGrafico.png'>";

}

function imprimirConsultaBarra($datos, $nombreGrafico, $nombresX ,$descripcionDatos, $tituloX, $tituloY)
{
	global $_PATH_LOCAL;
	global $_PATH_WEB;

	$color = array();

	$color[0] = "#003f7d";
	$color[1] = "#00b95c";
	$color[2] = "#ffff6a";

	$color[3] = "#006633";
	$color[4] = "#8cc6ff";
	$color[5] = "#cb9900";

	$grafico = new Graph(300,150, "auto");
	//$grafico->SetShadow();

	$grafico->SetScale( 'textlin');
	$grafico->title->Set($nombreGrafico);
	$grafico->img->SetMargin(40,20,30,30);
	$grafico->title->SetFont(FF_FONT1,FS_BOLD);
	$grafico->title->SetColor("black");

	for($i = 0; $i<count($datos); $i++)
	{
		$grupo[$i] = new BarPlot($datos[$i]);
		$grupo[$i]->SetWidth(0.6);
		$grupo[$i]->SetFillGradient($color[$i],$color[$i],GRAD_WIDE_MIDVER);
		$grupo[$i]->SetColor($color[$i]);
		$grupo[$i]->value->Show();

		if($descripcionDatos != "")
			$grupo[$i]->SetLegend($descripcionDatos[$i]);

	}

	$gbplot = new GroupBarPlot($grupo);

	$grafico->SetMarginColor('white');
	$grafico->SetColor('white');
	$grafico->xaxis->SetTickLabels($nombresX);

	$grafico->xaxis->title->Set($tituloX);
	$grafico->yaxis->title->Set($tituloY);
	$grafico->xaxis->SetColor("black","black");
	$grafico->yaxis->SetColor("black","black");

	$grafico->Add($gbplot);

	$name="$_PATH_LOCAL/TopLlamadas/graficas/grafico_barra_$nombreGrafico.png";
	$grafico->Stroke($name);

	return "<img alt='reporte' src='$_PATH_WEB/TopLlamadas/graficas/grafico_barra_$nombreGrafico.png'>";
}
?>
